<?php

require_once 'cache/policies/EntryKey.php';

/**
 * Add accessCount property to standard EntryKey
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see StandardPolicy
 * @see EntryKey
 * @date January 2009;
 */
class LFUEntryKey extends EntryKey {
	
	/**
	 * @var int accessCount counter of accesses.
	 */
	public $accessCount;
	
	/**
	 * Creates an LFUEntryKey Object.
	 * 
	 * @param key, value of the key.
	 */
	public function LFUEntryKey($key) {
		parent::EntryKey($key);
	}
	
	/**
	 * Print the value and the access count of EntryKey
	 */
	public function toString() {
		return "[" . $this->key . ", " + $this->accessCount . "]";
	}

}

?>
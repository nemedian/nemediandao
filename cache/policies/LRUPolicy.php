<?php

require_once 'cache/policies/StandardPolicy.php';

/**
 * It implements an Last Recentrly Used Policy
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see StandardPolicy
 * @see Policy
 * @date January 2009;
 */
class LRUPolicy extends StandardPolicy {
	
	/**
	 * Accessed element is put at the end of the list
	 * 
	 * @see Policy::accessKey()
	 */
	public function accessKey($key) {
		$eKey = $this->getKey($key);
		if ($eKey != null) {
			$eKey->remove();
			$eKey->addBefore($this->nil);
		}
	}
	
	/**
	 * nil key, to indicate the end of the list is returned.
	 * 
	 * @see StandardPolicy::keyToInsertBefore()
	 */
	protected function keyToInsertBefore() {
		return $this->nil;
	}

}

?>
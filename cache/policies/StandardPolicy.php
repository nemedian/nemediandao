<?php

require_once 'cache/policies/Policy.php';
require_once 'cache/policies/EntryKey.php';

/**
 * Defines a standard behaviour for a Policy. It manages two structures: one based on a
 * circular bidirectional list, the second one based on an array, to optimize key access in all usage
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see Policy
 * @see EntryKey
 * @date January 2009;
 */
abstract class StandardPolicy implements Policy {
	
	/**
	 * @var array where keys are stored.
	 */
	protected $keys = array();
	
	/**
	 * @var EntryKey first element and head of keys bidirectional list.
	 */
	protected $nil;
	
	/**
	 * Creates a StandardPolicy object;
	 */
	public function StandardPolicy() {
		$this->nil = $this->createEntryKey(null);
	}
	
	/**
	 * Creates an EntryKey object for given key.
	 *
	 * @param string key created.
	 */
	protected function createEntryKey($key) {
		return new EntryKey($key);
	}
	
	/**
	 * @see Policy::putKey()
	 */
	public function putKey($key) {
		$eKey = $this->createEntryKey($key);
		$this->keys[$eKey->key] = $eKey;
		$eKey->addBefore($this->keyToInsertBefore());
	}
	
	/**
	 * @see Policy::removeKey()
	 */
	public function removeKey() {
		$key = $this->nil->after->remove()->key;
		unset($this->keys[$key]);
		return $key;
	}
	
	/**
	 * @see Policy::containsKey()
	 */
	public function containsKey($key) {
		return $this->getKey($key) != null;
	}
	
	/**
	 * @see Policy::getKey()
	 */
	public function getKey($key) {
		if (array_key_exists($key, $this->keys)) {
			return $this->keys[$key];
		} else
			return null;
	}
	/**
	 * @see Policy::clear()
	 */
	public function clear() {
		$this->nil->before = $this->nil->after = $this->nil;
		return empty($this->keys);
	}
	
	/**
	 * Subclasses must implements this method and return an element of the bidirectional list to insert the new element before
	 * The implementation of this method implements the policy rule.
	 * Implementing this method, in fact, object chose order of elements. First element in the order is the element chosen for deletion
	 * where it is necessary.
	 *
	 * @return EntryKey key to insert the managed key before..
	 */
	protected abstract function keyToInsertBefore();
	
	/**
	 * String returned depends by EntryKey __toString() method implementations.
	 *
	 * @return string policy serializated.
	 */
	public function __toString() {
		$toReturn = "queue[";
		for ($eKey = $this->nil->after; $eKey !== $this->nil; $eKey = $eKey->after) {
			$toReturn .= $eKey;
		}
		return $toReturn . "], keys[" . $this->keys . "]";
	}

}

?>

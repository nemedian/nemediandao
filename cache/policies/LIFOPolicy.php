<?php

require_once 'cache/policies/StandardPolicy.php';

/**
 * It implements an Last In First Out Policy
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see StandardPolicy
 * @see Policy
 * @date January 2009;
 */
class LIFOPolicy extends StandardPolicy {
	
	/**
	 * If a key is accessed no modification is made on list
	 * 
	 * @see Policy::accessKey()
	 */
	public function accessKey($key) {
		return;
	}
	
	/**
	 * Return the begin of the list.
	 * 
	 * @see StandardPolicy::keyToInsertBefore()
	 */
	protected function keyToInsertBefore() {
		return $this->nil->after;
	}

}

?>
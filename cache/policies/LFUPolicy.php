<?php

require_once 'cache/policies/LFUEntryKey.php';
require_once 'cache/policies/StandardPolicy.php';

/**
 * It implements an Last Frequently Used Policy
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see StandardPolicy
 * @see Policy
 * @date January 2009;
 */
class LFUPolicy extends StandardPolicy {
	
	/**
	 * An extended EntryKey is necessary to manage accessCount property
	 * 
	 * @see StandardPolicy::createEntryKey()
	 */
	protected function createEntryKey($key) {
		return new LFUEntryKey($key);
	}
	
	/**
	 * Modify access count of the key and reorder list on it
	 * 
	 * @see Policy::accessKey()
	 */
	public function accessKey($key) {
		
		$accessedKey = $this->getKey($key);
		
		if ($accessedKey != null) {
			
			$accessedKey->accessCount++;
			
			$beforeKey = null;
			for ($eKey = $accessedKey->after; $accessedKey->accessCount > $eKey->accessCount && $eKey != $this->nil; $eKey = $eKey->after) {
				$beforeKey = $eKey;
			}
			
			if ($beforeKey != null) {
				$accessedKey->remove();
				$accessedKey->addBefore($beforeKey->after);
			}
		
		}
	}
	
	/**
	 * Return the begin of the list.
	 * 
	 * @see StandardPolicy::keyToInsertBefore()
	 */
	protected function keyToInsertBefore() {
		return $this->nil->after;
	}

}

?>
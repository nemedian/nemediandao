<?php

/**
 * Policies manages a structure of keys, providing reading and writing methods to do this..
 * It implements logics to determine removing of keys.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policies;
 * @since PHP 5.1;
 * @date January 2009;
 */
interface Policy {
	
	/**
	 * Insert a key in policy
	 *
	 * @param string key to insert;
	 */
	function putKey($key);
	
	/**
	 * Remove a key chosen by politics implemented by policy object. Removed key is returned.
	 *
	 * @return string key if a key is removed, null anyway.
	 */
	function removeKey();
	
	/**
	 * Return true if key is present in policy, false otherwise.
	 *
	 * @param string key to look for;
	 * @return boolean true if key is present in policy, false otherwise.
	 */
	function containsKey($key);
	
	/**
	 * Method invoked when an element key is accessed.
	 *
	 * @param string accessed key;
	 */
	function accessKey($key);
	
	/**
	 * Empty policy
	 */
	function clear();

}

?>
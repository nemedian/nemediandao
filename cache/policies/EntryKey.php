<?php

// FIXME questa classe rappresenta una generica lista circolare. Potrebbe essere spostata in un package pi� generico.
/**
 * Element of a bidirectional list.
 * It has a predecessor,a successor and a key value.
 * It is possibile remove it from the list or add it linking to new list.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>;
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @date January 2009;
 */
class EntryKey {
	
	/**
	 * @var string key value.
	 */
	public $key;
	
	/**
	 * @var EntryKey key predecessor and successor
	 */
	public $before, $after;
	
	/**
	 * Creates an EntryKey Object. It's its successor and its predecessor
	 * 
	 * @param key, value of the key.
	 */
	public function EntryKey($key) {
		$this->key = $key;
		$this->after = $this;
		$this->before = $this;
	}
	
	/**
	 * Remove key from the list and return it.
	 */
	public function remove() {
		$this->before->after = $this->after;
		$this->after->before = $this->before;
		return $this;
	}
	
	/**
	 * Link the element before the given elemen to add to the list
	 *
	 * @param EntryKey key to ad before.
	 */
	public function addBefore(EntryKey $existingEntry) {
		$this->after = $existingEntry;
		$this->before = $existingEntry->before;
		$this->before->after = $this;
		$this->after->before = $this;
	}
	
	/**
	 * Print the value of the EntryKey
	 */
	public function __toString() {
		return "[" . $this->key . "]";
	}

}

?>
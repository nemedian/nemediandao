<?php

require_once 'cache/policies/LFUPolicy.php';

/**
 * It implements an Last Frequently Used Policy, with the difference that a new element is inserted
 * at the end and not at the begin of the list
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see LFUPolicy
 * @see StandardPolicy
 * @see Policy
 * @date January 2009;
 */
class LFUInsertPriorityPolicy extends LFUPolicy {
	
	/**
	 * Return the end of the list.
	 * 
	 * @see LFUPolicy::keyToInsertBefore()
	 */
	protected function keyToInsertBefore() {
		return $this->nil;
	}

}

?>
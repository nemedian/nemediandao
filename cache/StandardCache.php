<?php

require_once 'cache/CacheException.php';
require_once 'cache/policies/Policy.php';
require_once 'cache/storages/Storage.php';
require_once 'cache/Cache.php';

/**
 * Standard cache implementation; it correctly implements cuncurrent updating of policy and storage.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package common;
 * @subpackage cache;
 * @since PHP 5.1;
 * @see Cache
 * @see Policy
 * @see Storage
 * @date January 2009;
 */
class StandardCache implements Cache {
	
	/**
	 * @var Policy to manage cache values deletion.
	 */
	private $policy;
	
	/**
	 * @var Storage where data are physically stored.
	 */
	private $storage;
	
	/**
	 * Creates a StandardCache Object based on given Storage and Policy;
	 *
	 * @param Storage $storage where data are physically stored.
	 * @param Policy $policy to manage cache values deletion.
	 */
	public function __construct(Storage $storage, Policy $policy) {
		$this->policy = $policy;
		$this->storage = $storage;
	}
	
	/**
	 * @see Cache::put()
	 */
	public function put($key, $value) {
		
		if ($key === null) {
			throw new CacheException("Cannot insert null keys. key[" + $key + "], value[" + $value + "].");
		}
		
		if ($this->storage->getCapacity() <= 0) {
			return null;
		}
		
		if (! $this->policy->containsKey($key)) {
			if ($this->storage->getSize() >= $this->storage->getCapacity()) {
				$rKey = $this->policy->removeKey();
				$this->storage->removeValue($rKey);
			}
			$this->policy->putKey($key);
		}
		
		return $this->storage->putValue($key, $value);
	
	}
	
	/**
	 * @see Cache::get()
	 */
	public function get($key) {
		$this->policy->accessKey($key);
		return $this->storage->getValue($key);
	}
	
	/**
	 * @see Cache::containsKey()
	 */
	public function containsKey($key) {
		return $this->storage->containsKey($key);
	}
	
	/**
	 * @see Cache::clear()
	 */
	public function clear() {
		$this->policy->clear();
		$this->storage->clear();
	}
	
	/**
	 * String returned depends by Storage and Policy __toString() method implementations.
	 *
	 * @return string cache serializated.
	 */
	public function __toString() {
		return "policy[" . $this->policy->__toString() . "] - storage[" . $this->storage->__toString() . "]";
	}

}

?>
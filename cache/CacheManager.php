<?php

require_once 'cache/StandardCache.php';
require_once 'cache/CacheException.php';
require_once 'db/Factory.php';
require_once 'log4php/LoggerManager.php';
require_once 'cache/CacheLoader.php';

/**
 * Manager for Cache objects; this object controls instantiation and ifecycle of Cache objects.
 * At the moment only two different lifecycles are implemented.
 * - INSTANCE: in which cache data do not survive to cache object
 * - REQUEST: in wich cache data lasts for the entire request
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package common;
 * @subpackage cache;
 * @since PHP 5.1;
 * @date January 2008;
 */
class CacheManager {
	
	/**
	 * @var string constant indicating INSTANCE lifecycle
	 */
	private static $INSTANCE = "INSTANCE";
	
	/**
	 * @var string constant indicating REQUEST lifecycle
	 */
	private static $REQUEST = "REQUEST";
	
	/**
	 * @var string name of the default cache
	 */
	private static $DEFAULT = "DEFAULT";
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * @var array. It contains instances of CacheManager class. That's an associative array, with keys
	 * representing cache config file and value representing cache manager for that file.
	 */
	private static $instances = array();
	
	/**
	 * @var array containing cache definitions read by confgi file.
	 */
	private $cacheDefinitions;
	
	/**
	 * @var array containing cache instances with REQUEST lifecycle.
	 */
	private $requestCaches = array();
	
	/**
	 * Constructor: Creates a CacheManager;
	 */
	private function __construct() {
		$defaultCacheDefinition = $this->createDefaultCacheDefinition();
		$this->cacheDefinitions[self::$DEFAULT] = $defaultCacheDefinition;
	}
	
	/**
	 * return a default CacheDefinition, represented by an associative array to associate to default value
	 * 
	 * @return array associative array
	 */
	private function createDefaultCacheDefinition() {
		
		static $DEFAULT_POLICY_IMPLEMENTATION = "common/cache/policies/LRUPolicy";
		
		static $DEFAULT_STORAGE_IMPLEMENTATION = "common/cache/storages/ArrayStorage";
		
		$defaultCacheDefinition = array();
		$defaultCacheDefinition[CacheLoader::$SCOPE] = self::$INSTANCE;
		$defaultCacheDefinition[CacheLoader::$POLICY][CacheLoader::$IMPLEMENTATION] = $DEFAULT_POLICY_IMPLEMENTATION;
		$defaultCacheDefinition[CacheLoader::$STORAGE][CacheLoader::$IMPLEMENTATION] = $DEFAULT_STORAGE_IMPLEMENTATION;
		$defaultCacheDefinition[CacheLoader::$STORAGE][CacheLoader::$CAPACITY] = 0;
		
		return $defaultCacheDefinition;
	}
	
	/**
	 * Static method that return the unique instance of CacheManager associated
	 * to given cache config file path; if the CacheManager for that file doesn't exists,
	 * an instance is automatically created and added in the DaoManager's associative array
	 *
	 * @param string $cacheConfigPath: represent the cache config file path for which a DaoManager is requested;
	 * @return CacheManager associated to given cache config file path;
	 */
	public static function getInstance($cacheConfigPath) {
		
		try {
			if (self::$logger == null) {
				self::$logger = & LoggerManager::getLogger(__CLASS__);
			}
			// check if an instance of CacheManager for given path already exists
			if (! array_key_exists($cacheConfigPath, self::$instances)) {
				
				// instantiate a new CacheManager
				$instance = new self();
				
				// parse given cache config path
				$cacheLoader = new CacheLoader($cacheConfigPath);
				$instance->cacheDefinitions = array_merge($instance->cacheDefinitions, $cacheLoader->getCacheDefinitions());
				
				// register new instance in CacheManager instance arrays
				self::$instances[$cacheConfigPath] = $instance;
			}
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new CacheException("Error getting " . __CLASS__ . " instance: $cacheConfigPath");
		}
		
		//return instance requested
		return self::$instances[$cacheConfigPath];
	
	}
	
	/**
	 * Method that return a Cache given its name and its id;
	 * If name or id does not macth any Cache definition, a default cache, not able
	 * to store any data, is returned and a warn is logged.
	 *
	 * @param string $name: name of the Cache requested, defined in config file
	 * @param string $id: id for that definition of cache
	 * @return Cache: cache requested
	 */
	public function getCache($name, $id = null) {
		
		$cache = null;
		try {
			if (array_key_exists($name, $this->cacheDefinitions)) {
				
				$cacheDefinition = $this->cacheDefinitions[$name];
				
				if ("REQUEST" == $cacheDefinition[CacheLoader::$SCOPE]) {
					
					$cache = $this->searchCache($cacheDefinition, $this->requestCaches, $name, $id);
				
				} else { // caso INSTANCE
				// do nothing
				}
			} else {
				self::$logger->warn("Requested cache is not configurated. name[" + $name + "].");
				return $this->instanceCache($this->cacheDefinitions[self::$DEFAULT], self::$DEFAULT);
			}
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new CacheException("Error getting cache: name[" . $name . "], id[" . $id . "]");
		}
		
		return $cache;
	
	}
	
	/**
	 * Search in alredy istantiated caches.
	 *
	 * @param array $cacheDefinition: array containing definition of requested Cache
	 * @param array &$caches: array to search instances in.
	 * @param string $name: name of the Cache requested, defined in config file
	 * @param string $id: id for that definition of cache
	 * @return Cache: cache requested
	 */
	private function searchCache(array $cacheDefinition, array &$caches, $name, $id) {
		
		if (array_key_exists($name, $caches) && array_key_exists($id, $caches[$name])) {
			return $caches[$name][$id];
		} else {
			$cache = $this->instanceCache($cacheDefinition, $name, $id);
			$caches[$name][$id] = $cache;
		}
		
		return $cache;
	}
	
	/**
	 * Instantiate a cache object based on given definition.
	 *
	 * @param array $cacheDefinition: array containing definition of requested Cache
	 * @return Cache: cache instantiated
	 */
	private function instanceCache(array $cacheDefinition) {
		
		$policyImpl = $cacheDefinition[CacheLoader::$POLICY][CacheLoader::$IMPLEMENTATION];
		$policy = Factory::newInstance($policyImpl);
		
		$storageImpl = $cacheDefinition[CacheLoader::$STORAGE][CacheLoader::$IMPLEMENTATION];
		$storageCapacity = $cacheDefinition[CacheLoader::$STORAGE][CacheLoader::$CAPACITY];
		$storage = Factory::newInstance($storageImpl, array($storageCapacity));
		
		return new StandardCache($storage, $policy);
	
	}

}
?>
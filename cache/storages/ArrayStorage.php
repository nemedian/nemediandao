<?php

require_once 'cache/storages/Storage.php';

/**
 * It implements a Storage based on an array;
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage policy;
 * @since PHP 5.1;
 * @see Storage
 * @date January 2009;
 */
class ArrayStorage implements Storage {
	
	/**
	 * @var int capacity of the storage; it must be >= 0;
	 */
	private $capacity;
	
	/**
	 * @var array where values are stored.
	 */
	private $entries = array();
	
	/**
	 * Creates a ArrayStorage Object;
	 */
	public function ArrayStorage($capacity) {
		$this->capacity = ($capacity < 0 ? 0 : $capacity);
	}
	
	/**
	 * @see Storage::getCapacity()
	 */
	public function getCapacity() {
		return $this->capacity;
	}
	
	/**
	 * @see Storage::getSize()
	 */
	public function getSize() {
		return count($this->entries);
	}
	
	/**
	 * @see Storage::getValue()
	 */
	public function getValue($key) {
		if (array_key_exists($key, $this->entries)) {
			return $this->entries[$key];
		} else
			return null;
	}
	
	/**
	 * @see Storage::putValue()
	 */
	public function putValue($key, $value) {
		return $this->entries[$key] = $value;
	}
	
	/**
	 * @see Storage::removeValue()
	 */
	public function removeValue($key) {
		unset($this->entries[$key]);
	}
	
	/**
	 * @see Storage::containsKey()
	 */
	public function containsKey($key) {
		return array_key_exists($key, $this->entries);
	}
	
	/**
	 * @see Storage::clear()
	 */
	public function clear() {
		return empty($this->entries);
	}
	
	/**
	 * @return string entries
	 */
	public function __toString() {
		return "entries: " . $this->entries;
	}

}

?>

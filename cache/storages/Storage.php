<?php

/**
 * It represent support on which is possible to save data.
 * It provides methods for reading and writing data.
 * Storage can be queried about its status composed by its capacity and its actual size.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package cache;
 * @subpackage storages;
 * @since PHP 5.1;
 * @date January 2009;
 */
interface Storage {
	
	/**
	 * Insert a value in storage associated to its key.
	 *
	 * @param string key of the element;
	 * @param string value value of the element associated to the given key
	 * @return previous value associated to given key, or null if key was not alredy present.
	 * A null return can indicate that null was really associated to given key
	 *	
	 */
	function putValue($key, $value);
	
	/**
	 * Return value associated to given key if it is present in storage. Otherwise null is returned.
	 *
	 * @param string key of the requested element;
	 * @return string value of the element associated to the given key
	 */
	function getValue($key);
	
	/**
	 * Remove value associated to given key if it is present in storage. Otherwise null happens.
	 *
	 * @param string key of the element to remove;
	 * @return string value of the element associated to the given key
	 */
	function removeValue($key);
	
	/**
	 * Return true if key is present in storage, false otherwise.
	 *
	 * @param string key to look for;
	 * @return boolean true if key is present in storage, false otherwise.
	 */
	function containsKey($key);
	
	/**
	 * Empty storage
	 */
	function clear();
	
	/**
	 * Return capacity of the storage.
	 *
	 * @return int capacity of the storage.
	 */
	function getCapacity();
	
	/**
	 * Return actual size of the storage. Size depends from number of value actually present in storage.
	 * Size cannot be more than capacity.
	 *
	 * @return int actual size of the storage.
	 */
	function getSize();

}

?>
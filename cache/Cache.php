<?php

/**
 * Interface that defines cache operations.
 * It defines inserting, reading, and cheching ov values.
 * Mass deleting is provided by method clear.
 * Single value deletion is not explicitly possible but delegated to externals controllers.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package common;
 * @subpackage cache;
 * @since PHP 5.1;
 * @date January 2009;
 */
interface Cache {
	
	/**
	 * Insert a value in cache associated to its key.
	 * Null values for $key are not allowed.
	 *
	 * @param string key of the element;
	 * @param string value value of the element associated to the given key
	 * @return previous value associated to given key, or null if key was not alredy present.
	 * A null return can indicate that null was really associated to given key
	 *	
	 */
	function put($key, $value);
	
	/**
	 * Return value associated to given key if it is present in cache. Otherwise null is returned.
	 *
	 * @param string key of the requested element;
	 * @return string value value of the element associated to the given key
	 */
	function get($key);
	
	/**
	 * Return true if ke is present in cache, false otherwise.
	 *
	 * @param string key to look for;
	 * @return boolean true if key is present in cache, false otherwise.
	 */
	function containsKey($key);
	
	/**
	 * Empty cache
	 */
	function clear();

}

?>

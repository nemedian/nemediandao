<?php

/**
 * Exception thrown if an error concerning cache is raised
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.0;
 * @package common;
 * @subpackage cache;
 * @since PHP 5.1;
 * @see Exception
 * @date January 2009;
 */
class CacheException extends Exception {
	
	/**
	 * Creates a CacheException object;
	 * 
	 * @param string message of the exception
	 * @param int code of the exception
	 */
	public function __construct($message, $code = 0) {
		// make sure everything is assigned properly
		parent::__construct($message, $code);
	}
	
	/**
	 * Return a string with message and code serialization of the exception.
	 */
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}

}
?>
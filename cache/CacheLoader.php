<?php

require_once 'cache/Cache.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoException.php';
require_once 'db/Factory.php';

/**
 * It's the loader class to parse a Cache configuration file.
 * It reads the source path file, reading defined cache classes, putting that in a map ready to be access
 * when a Cache instance is required
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package common;
 * @subpackage cache;
 * @since PHP 5.1;
 * @date January 2008;
 * 
 * @example
 * <?xml version="1.0" encoding="UTF-8"?>
 * <config>
 * 	<caches>
 * 		<cache name="MemoizerProxy" scope="REQUEST">
 * 			<policy implementation="common/cache/policies/LRUPolicy" />
 * 			<storage capacity="200"	implementation="common/cache/storages/ArrayStorage" />
 * 		</cache>
 * 	</caches>
 * </config>
 */
class CacheLoader {
	
	/**
	 * @var array array containing values read from xml;
	 */
	private $cacheDefinitions = array();
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * @var element 'name' costant in the xml;
	 */
	public static $NAME = 'name';
	
	/**
	 * @var element 'scope' costant in the xml;
	 */
	public static $SCOPE = 'scope';
	
	/**
	 * @var element 'policy' costant in the xml;
	 */
	public static $POLICY = 'policy';
	
	/**
	 * @var element 'implementation' costant in the xml;
	 */
	public static $IMPLEMENTATION = 'implementation';
	
	/**
	 * @var element 'storage' costant in the xml;
	 */
	public static $STORAGE = 'storage';
	
	/**
	 * @var element 'capacity' costant in the xml;
	 */
	public static $CAPACITY = 'capacity';
	
	/**
	 * Constructor: Creates a CacheLoader based on given file path;
	 *
	 * @param String $filePath cache config file path containing information about caches;
	 */
	public function __construct($filePath) {
		
		if (self::$logger == null) {
			self::$logger = LoggerManager::getLogger(__CLASS__);
		}
		
		// parsing dao config file
		try {
			
			// reading file
			self::$logger->info("Reading Cache config file: " . $filePath);
			$doc = new DOMDocument();
			$doc->load($filePath);
			$this->readCacheDefinitions($doc);
		
		} catch (Exception $e) {
			$message = "Error reading cacheConfig file: $filePath";
			self::$logger->error($e->getMessage());
			throw new CacheException($message);
		}
	
	}
	
	/**
	 * Creates a CacheLoader object;
	 * 
	 * @return array cache definitions read from xml;
	 */
	public function getCacheDefinitions() {
		return $this->cacheDefinitions;
	}
	
	/**
	 * Read and parse cache config file
	 * setting read information in the $this->cacheDefinitions variable;
	 *
	 * @param DOMDocument $doc: its the xml representation for the dao config;
	 */
	private function readCacheDefinitions($doc) {
		
		// select caches tag
		$cachesNode = $doc->getElementsByTagName("caches")->item(0);
		
		// select cache tags
		$cacheNodes = $cachesNode->getElementsByTagName("cache");
		
		foreach ($cacheNodes as $cacheNode) {
			
			$name = $cacheNode->getAttribute(self::$NAME);
			$this->cacheDefinitions[$name][self::$SCOPE] = $cacheNode->getAttribute(self::$SCOPE);
			
			$policyNodeList = $cacheNode->getElementsByTagName("policy");
			if ($policyNodeList->length != 0) {
				$this->cacheDefinitions[$name][self::$POLICY][self::$IMPLEMENTATION] = $policyNodeList->item(0)->getAttribute(self::$IMPLEMENTATION);
			}
			
			$storageNodeList = $cacheNode->getElementsByTagName("storage");
			if ($storageNodeList->length != 0) {
				$this->cacheDefinitions[$name][self::$STORAGE][self::$IMPLEMENTATION] = $storageNodeList->item(0)->getAttribute(self::$IMPLEMENTATION);
				$this->cacheDefinitions[$name][self::$STORAGE][self::$CAPACITY] = $storageNodeList->item(0)->getAttribute(self::$CAPACITY);
			}
		
		}
	
	}

}
?>
<?php

require_once 'mutex/Mutex.php';
require_once 'mutex/MutexException.php';

abstract class StandardMutex implements Mutex {

    private static $logger;
    
    protected $mutexId;

    public function __construct(MutexIdentity $mutexId) {
        $this->mutexId = $mutexId;
    }
    
}
?>

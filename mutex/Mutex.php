<?php
/**
 * Simple interface for simple mutal exclusion locks. Each acquire
 * get the lock and each release, release it. Realising a not aquired lock has not effect.
 * 
 * <h3>example</h3>
 * 
 * <pre>
 * $mutex = new Mutex();
 * try {
 *     $mutex->acquire();
 *     try {
 *         // critical section code
 *     } finally {
 *         $mutex->release();
 *     }
 * } catch (MutexInterruptedException $e) {
 *     // error handling
 *     // not need to invoke release method
 * }
 */
interface Mutex {
	
	/**
	 * Wait (potentially forever) until lock is acquired. It fails in case of interruption only.
	 * A normal return guarantee acquiring has been executed successfully.
	 * 
	 * @throws MutexInterruptedException
	 *                 If is not possible to acquire lock.
	 */
	function acquire();
	
	/**
	 * Release lock. Releasing a not acuired lock has not effect
	 */
	function release();
}

?>
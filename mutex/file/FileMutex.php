<?php

require_once 'log4php/LoggerManager.php';
require_once 'mutex/MutexException.php';
require_once 'mutex/StandardMutex.php';
require_once 'mutex/Mutex.php';
require_once 'mutex/MutexIdentity.php';

/**
 * LOCK_PATH needed to indicate location to create lock files
 */
class FileMutex extends StandardMutex implements Mutex {
	
	private static $logger;
	
	private $fp;
	
	private $fileName;
	
	public function __construct(MutexIdentity $mutexId) {
		parent::__construct($mutexId);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->fileName = LOCK_PATH . '/' . base_convert($mutexId, 16, 16) . '.lock';
	
	}
	
	public function acquire() {
		try {
			$this->fp = @fopen($this->fileName, 'w');
			@flock($this->fp, LOCK_EX);
		} catch (Exception $e) {
			$this->release();
			throw new MutexException($e->getMessage());
		}
	}
	
	public function release() {
		try {
			@fclose($this->fp);
			@unlink($this->fileName);
		} catch (Exception $e) {
			self::$logger->error($e);
		}
	}

}
?>
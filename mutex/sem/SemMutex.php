<?php

require_once 'log4php/LoggerManager.php';
require_once 'common/mutex/MutexException.php';
require_once 'common/mutex/StandardMutex.php';
require_once 'common/mutex/Mutex.php';
require_once 'common/mutex/MutexIdentity.php';

/**
 * LOCK_PATH needed to indicate location to create lock files
 * 
 * FIXME: nella release dovrebbe essere fatta la rimozione dei semafori. Questo al momento non si riesce a fare
 * perch� la rimozione deve essere eseguita solo se sono l'unico processo a usare il semaforo, ma non esiste niente per sapere questo.
 * Anche perch� esiste un numero massimo di semafori.
 * 
 * FIXME: stringhe troppo lunghe insistono sullo stesso semaforo 0x800000000. E' necessario quindi codificare con un metodo di hashing
 * le stringhe in modo che rientrino nel range dei semafori. Questo tuttavia potrebbe creare collisioni e possibili deadlock in un uso spinto
 * Essendo il rischio relativamente ridotto, la soluzione dell'hashing pare essere la migliore.
 */
class SemMutex extends StandardMutex implements Mutex {
	
	private static $logger;
	
	private $sem_id;
	
	public function __construct(MutexIdentity $mutexId) {
		parent::__construct($mutexId);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		// get semaphore identifier
		$this->sem_id = sem_get(base_convert($mutexId->__toString(), 32, 10), 1, 0666, true);
	}
	
	public function acquire() {
		try {
			// acquire semaphore lock
			sem_acquire($this->sem_id);
		} catch (Exception $e) {
			$this->release();
			throw new MutexException($e->getMessage());
		}
	}
	
	public function release() {
		try {
			// release semaphore lock
			sem_release($this->sem_id);
		} catch (Exception $e) {
			self::$logger->error($e);
		}
	}

}
?>
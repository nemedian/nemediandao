<?php

require_once 'mutex/file/FileMutex.php';
require_once 'log4php/LoggerManager.php';
require_once 'mutex/MutexIdentity.php';

class Locker {
	
	private static $logger;
	
	private static $instance;
	
	private static $mutexStack = array();
	
	public static function getInstance() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		if (self::$instance == null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function lock(MutexIdentity $mutexId) {
		
		// acquire mutex resource and start critical section
		if (! array_key_exists($mutexId->__toString(), self::$mutexStack)) {
			$mutex = new FileMutex($mutexId);
			$mutex->acquire();
			self::$mutexStack[$mutexId->__toString()]['object'] = $mutex;
			self::$mutexStack[$mutexId->__toString()]['count'] = 1;
			self::$logger->debug("Resource \"" . $mutexId->__toString() . "\" LOCKED.");
		} else {
			self::$mutexStack[$mutexId->__toString()]['count'] = ++self::$mutexStack[$mutexId->__toString()]['count'];
			self::$logger->debug("Resource \"" . $mutexId->__toString() . "\" increased lock count to: " . self::$mutexStack[$mutexId->__toString()]['count']);
		}
	
	}
	
	public function unlock(MutexIdentity $mutexId) {
		
		// release mutex resource and close critical section
		if (array_key_exists($mutexId->__toString(), self::$mutexStack) && self::$mutexStack[$mutexId->__toString()]['count'] == 1) {
			$mutex = self::$mutexStack[$mutexId->__toString()]['object'];
			$mutex->release($mutexId);
			unset(self::$mutexStack[$mutexId->__toString()]);
			self::$logger->debug("Resource \"" . $mutexId->__toString() . "\" UNLOCKED.");
		} else {
			self::$mutexStack[$mutexId->__toString()]['count'] = self::$mutexStack[$mutexId->__toString()]['count'] - 1;
			self::$logger->debug("Resource \"" . $mutexId->__toString() . "\" decreased lock count to: " . self::$mutexStack[$mutexId->__toString()]['count']);
		}
	
	}
	
	public function __destruct() {
		
		foreach (array_keys(self::$mutexStack) as $id) {
			$this->unlock(new MutexIdentity($id));
		}
	
	}

}

?>

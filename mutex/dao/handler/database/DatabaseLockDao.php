<?php
/**
 * --
 * -- Table structure for table `lock` needed to use this dao implementation
 * --
 * 
 * CREATE TABLE `lock` (
 *   `id` varchar(64) NOT NULL,
 *   PRIMARY KEY  (`id`)
 * )
 */

require_once 'mutex/dao/handler/LockDao.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/QueryRunner.php';
require_once 'dbo/Dao.php';

class DatabaseLockDao extends Dao implements LockDao {
	
	private static $DAO_SQL_PATH = "/sql/DatabaseLockDao.xml";
	
	//SQL STATEMENT query keys
	private static $LOCK = "lock";
	
	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(dirname(__FILE__) . self::$DAO_SQL_PATH);
	}
	
	public function lock($id) {
		$args = array($id);
		return $this->execute($this->getStatement(self::$LOCK), $args);
	}
}
?>
<?php

require_once 'db/IDao.php';

interface LockDao extends IDao {
	
	function lock($id);
	
}
?>
<?php

require_once 'log4php/LoggerManager.php';
require_once 'mutex/MutexException.php';
require_once 'mutex/StandardMutex.php';
require_once 'mutex/Mutex.php';
require_once 'mutex/dao/handler/LockDao.php';
require_once 'db/DaoManager.php';
require_once 'mutex/MutexIdentity.php';

/**
 * MUTEX_DAO_CONFIG_PATH needed to indicate dao config to use
 */
class DaoMutex extends StandardMutex implements Mutex {

    private static $logger;
    
    private $daoManager;
    
    public function __construct(MutexIdentity $mutexId) {
        parent::__construct($mutexId);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
    	if ($this->daoManager == null){
			$this->daoManager = DaoManager::newInstance(MUTEX_DAO_CONFIG_PATH);
		}
    }

    public function acquire() {
        try {
            $lockDao = $this->daoManager->getDao("LockDao");
            $this->daoManager->startTransaction();
            $lockDao->lock($this->mutexId->__toString());
        } catch (Exception $e) {
            $this->release();
            throw new MutexException($e->getMessage());
        }
    }

    public function release() {
        try {
            $this->daoManager->rollbackTransaction();
        } catch (Exception $e) {
            self::$logger->error($e);
        }
    }

}
?>
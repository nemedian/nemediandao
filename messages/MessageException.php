<?php

if (!defined('ERROR_BUNDLE')){
	define('ERROR_BUNDLE', dirname(__FILE__)."/error.properties");
}

require_once 'messages/ErrorMessage.php';

class MessageException extends Exception {

	protected $message;

	public function __construct(ErrorMessage $message = null) {

		static $INTERNAL_ERROR = "internal.error";

		if ($message == null){
			$message = new ErrorMessage(ERROR_BUNDLE, $INTERNAL_ERROR);
		}

		$this->message = $message;
	}

}
?>
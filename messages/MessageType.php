<?php

require_once 'messages/Enum.php';

final class MessageType extends Enum {
	
	private static $INFO;
	
	private static $ERROR;
	
	private static $WARN;
	
	protected function __construct() {
	}
	
	public static function INFO() {
		if (self::$INFO == null) {
			self::$INFO = parent::init(__CLASS__, "INFO");
		}
		return self::$INFO;
	}
	
	public static function ERROR() {
		if (self::$ERROR == null) {
			self::$ERROR = parent::init(__CLASS__, "ERROR");
		}
		return self::$ERROR;
	}
	
	public static function WARN() {
		if (self::$WARN == null) {
			self::$WARN = parent::init(__CLASS__, "WARN");
		}
		return self::$WARN;
	}

}
?>

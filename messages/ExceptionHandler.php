<?php

require_once 'messages/MessageException.php';
require_once 'messages/Message.php';
require_once 'messages/MessageDecoder.php';
require_once 'log4php/LoggerManager.php';
require_once 'messages/BundleManager.php';

class ExceptionHandler {
	
	private static $logger;
	
	private static $bundleInit;
	
	private static $instance;
	
	private $checkSuppressedFlag;
	
	private function __construct() {
		if (self::$logger == null) {
			self::$logger = LoggerManager::getLogger(__CLASS__);
		}
		if (self::$bundleInit == null) {
			self::$bundleInit = BundleManager::getInstance()->addBundle(ERROR_BUNDLE);
		}
	}
	
	public static function init() {
		if (self::$instance == null) {
			self::$instance = new self();
			set_error_handler(array(self::$instance, 'handleError'), E_ALL);
			set_exception_handler(array(self::$instance, 'handleException'));
			register_shutdown_function(array(self::$instance, 'shutdown'));
		}
	}
	
	public function handleError($errno, $errstr, $errfile, $errline) {
		
		$message = $errno . " - " . $errstr . " - " . $errfile . " - " . $errline;
		
		// don't throw exception to the error if it
		// was suppressed with a '@'
		if (error_reporting() == 0) {
			self::$logger->warn("Error suppressed: " . $message);
		} else {
			self::$logger->error($message);
			throw new Exception($message);
		}
	
	}
	
	public function handleException($exception) {
		
		$errno = $exception->getCode();
		$errfile = $exception->getFile();
		$errline = $exception->getLine();
		
		if ($exception instanceof MessageException) {
			$errstr = MessageDecoder::getProperty($exception->getMessage());
		} else {
			$errstr = $exception->getMessage();
		}
		
		$message = $errno . " - " . $errstr . " - " . $errfile . " - " . $errline;
		
		self::$logger->fatal($message);
		self::$logger->fatal("Trace:" . $this->getTraceAsString($exception));
		exit();
	}
	
	public function shutdown() {
		$lastError = error_get_last();
		if ($lastError != null) {
			self::handleError($lastError['type'], $lastError['message'], $lastError['file'], $lastError['line']);
		}
		self::$logger->info("Application stopped.");
	}
	
	private function getTraceAsString(Exception $e) {
		$trace = '';
		foreach ($e->getTrace() as $i => $item) {
			foreach ($item['args'] as $j => $arg)
				$item['args'][$j] = print_r($arg, true);
			$trace .= LOG4PHP_LINE_SEP . "#$i ";
			$trace .= (isset($item['class']) ? $item['class'] . $item['type'] : '') . $item['function'] . '(' . implode(', ', $item['args']) . ")";
			$trace .= (isset($item['file']) ? " at [" . $item['file'] . ":" . $item['line'] . "]" : '');
		}
		return $trace;
	}
	
	public function __destruct() {
		restore_error_handler();
		restore_exception_handler();
	}

}
?>
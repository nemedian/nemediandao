<?php

require_once 'messages/Message.php';
require_once 'messages/MessageType.php';

class ErrorMessage extends Message {

    public function __construct($bundle, $key, array $args = array()) {
    	parent::__construct(MessageType::ERROR(), $bundle, $key, $args);
    }

    public function setMessageType(MessageType $messageType) {
    	if (!(MessageType::ERROR() == $messageType) ){
    		throw new Exception();
    	}
    }

}
?>
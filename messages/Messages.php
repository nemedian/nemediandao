<?php

require_once 'messages/MessageType.php';
require_once 'messages/Message.php';

/**
 * Classe che gestisce una lista di oggetti Message
 */
class Messages {

	private $messages = array();

	public function __construct(array $messages = array()){
		foreach ($messages as $message)	$this->add($message);
	}
	
	public function add(Message $message) {
		array_push($this->messages, $message);
	}

	public function addAll(Messages $messages) {
		foreach ($messages->toArray() as $message)	$this->add($message);
	}

	public function toArray() {
		return $this->messages;
	}

	public function getMessagesOfType(MessageType $messageType){

		$messages = new Messages();

		foreach ($this->toArray() as $message){
			if ($messageType == $message->getMessageType()){
				$messages->add($message);
			}
		}
			
		return $messages;
	}

	public function setBundleToAll($bundle) {
		foreach ($this->toArray() as $message) $message->setBundle($bundle);
	}

	public function addArgToAll($arg) {
		foreach ($this->toArray() as $message) $message->addArg($arg);
	}

	public function addArgsToAll(array $args) {
		foreach ($this->toArray() as $message) $message->addAllArgs($args);
	}

	public function clear() {
		$this->messages = array();
	}

	public function isEmpty() {
		return empty($this->messages);
	}

}
?>

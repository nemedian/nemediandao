<?php
if (! defined('MESSAGE_DIR'))
	define('MESSAGE_DIR', dirname(__FILE__));

require_once (MESSAGE_DIR . "/MessageType.php");

/**
 * Classe utilizzabile per la gestione dei messaggi i18n.
 */
class Message {
	
	/**
	 * Nome logico del resource bundle ovvero l'alias utilizzato per
	 * identificare il file di risorse da utilizzare.
	 */
	private $bundle;
	
	/** Chiave corrispondente al messaggio localizzato. */
	private $key;
	
	/** Tipologia del messaggio. */
	private $messageType;
	
	/** Argomenti da utilizzare nella creazione del messaggio. */
	private $args = array();
	
	public function __construct(MessageType $messageType, $bundle, $key, array $args = array()) {
		$this->messageType = $messageType;
		$this->bundle = $bundle;
		$this->key = $key;
		foreach ($args as $arg)
			$this->addArg($arg);
	}
	
	public function addArg($arg) {
		array_push($this->args, $arg);
	}
	
	public function addArgs(array $args) {
		foreach ($args as $arg)
			$this->addArg($arg);
	}
	
	public function getArgs() {
		return $this->args;
	}
	
	public function setArgs(array $args) {
		$this->args = $args;
	}
	
	public function getKey() {
		return $this->key;
	}
	
	public function setKey($key) {
		$this->key = $key;
	}
	
	public function getMessageType() {
		return $this->messageType;
	}
	
	public function setMessageType(MessageType $messageType) {
		$this->messageType = $messageType;
	}
	
	public function getBundle() {
		return $this->bundle;
	}
	
	public function setBundle($bundle) {
		$this->bundle = $bundle;
	}
	
	public function __toString() {
		return 'bundle: ' . $this->bundle . ', key: ' . $this->key . ', messageType: ' . $this->messageType->getName() . ', args: [' . implode(", ", $this->args) . ']';
	}

}
?>

<?php
require_once 'messages/BundleManager.php';

abstract class MessageDecoder {

	private static $bundleManager;

	public static function getProperty(Message $message){
		if (self::$bundleManager == null){
			self::$bundleManager = BundleManager::getInstance();
		}
		return self::$bundleManager->getProperty($message->getBundle(), $message->getKey(), $message->getArgs());
	}

}
?>
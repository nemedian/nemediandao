<?php
require_once 'properties/Properties.php';
require_once 'log4php/LoggerManager.php';
/**
 * Semplice implementazione del design pattern singleton.
 * Data la struttura sintattica di PHP non è possibile sfruttare l'ereditarietà
 * per poter riutilizzare il pattern, se non utilizzando piccoli stratagemmi sintattici.
 */
class BundleManager {
	
	private static $logger;
	
	private $bundles = array();
	
	/**
	 * La variabile statica privata che conterrà l'istanza univoca
	 * della nostra classe.
	 */
	private static $instance = null;
	
	/**
	 * Il costruttore in cui ci occuperemo di inizializzare la nostra
	 * classe. E' opportuno specificarlo come privato in modo che venga
	 * visualizzato automaticamente un errore dall'interprete se si cerca
	 * di istanziare la classe direttamente.
	 */
	private function __construct() {
		// ... codice ...
	}
	
	/**
	 * Il metodo statico che si occupa di restituire l'istanza univoca della classe.
	 * per facilitare il riutilizzo del codice in altre situazioni, si frutta la
	 * costante __CLASS__ che viene valutata automaticamente dall'interprete con il
	 * nome della classe corrente (ricordo che "new $variabile" crea un'istanza della classe
	 * il cui nome è specificato come stringa all'interno di $variabile)
	 */
	public static function getInstance() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		if (self::$instance == null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function getProperty($bundlePath, $key, $args = null, $defaultValue = null) {
		
		$NOT_FOUND_MESSAGE = "??? $key ???";
		
		$message = ($defaultValue == null) ? $NOT_FOUND_MESSAGE : $key;
		
		if (defined("LOCALE_SUFFIX") && LOCALE_SUFFIX != '') {
			$bundlePathLang = self::appendSuffix($bundlePath, LOCALE_SUFFIX);
			if (array_key_exists($bundlePathLang, $this->bundles)) {
				self::$logger->debug("Request key $key in bundle $bundlePathLang");
				$message = $this->bundles[$bundlePathLang]->getProperty($key, $NOT_FOUND_MESSAGE);
			}
		}
		if ($message == $NOT_FOUND_MESSAGE && array_key_exists($bundlePath, $this->bundles)) {
			self::$logger->debug("Request key $key in bundle $bundlePath");
			$message = $this->bundles[$bundlePath]->getProperty($key, $NOT_FOUND_MESSAGE);
		}
		if ($message == $NOT_FOUND_MESSAGE) {
			self::$logger->warn("Message with key $key not found in bundle $bundlePath or bundle missing");
		} else {
			$message = $this->bindParameters($message, $args);
			self::$logger->debug("Message decoded: " . $message);
		}
		
		return $message;
	
	}
	
	// return true if given bundle is inserted
	public function addBundle($bundlePath) {
		// load generic bundle
		if (! array_key_exists($bundlePath, $this->bundles)) {
			$p = $this->loadBundle($bundlePath);
			if ($p != null)
				$this->bundles[$bundlePath] = $p;
		}
		
		// load bundle for setted language
		if (defined('LOCALE_SUFFIX') && LOCALE_SUFFIX != '') {
			$bundlePath = self::appendSuffix($bundlePath, LOCALE_SUFFIX);
			if (! array_key_exists($bundlePath, $this->bundles)) {
				$p = $this->loadBundle($bundlePath);
				if ($p != null)
					$this->bundles[$bundlePath] = $p;
			}
		}
		
		return $p != null;
	
	}
	
	// FIXME: carica anche i bundle vuoti!
	private function loadBundle($bundlePath) {
		$p = new Properties();
		
		self::$logger->debug("loading bundle: " . $bundlePath);
		try {
			if (file_exists($bundlePath)) {
				$p->load(file_get_contents($bundlePath));
			} else {
				self::$logger->warn("Unable to load bundle: " . $bundlePath);
				$p = null;
			}
		} catch (Exception $e) {
			self::$logger->warn("Unable to load bundle: " . $bundlePath);
		}
		return $p;
	}
	
	private static function appendSuffix($path, $suffix) {
		$lIdx = strrpos($path, ".");
		return substr($path, 0, $lIdx) . "_" . $suffix . substr($path, $lIdx);
	}
	
	private function bindParameters($message, $args) {
		$pattern = "'{[0-9]+}'";
		$matches = array();
		
		while (preg_match($pattern, $message, $matches, PREG_OFFSET_CAPTURE)) {
			$match = $matches[0][0];
			$arg = $args[substr($match, 1, strlen($match) - 2)];
			if (is_numeric($arg) || $arg != null) {
				$message = substr_replace($message, $arg, $matches[0][1], 3);
			} else {
				self::$logger->warn("Parameter " . $matches[0][0] . " not passed.");
				break;
			}
		}
		
		return $message;
	}

}
?>
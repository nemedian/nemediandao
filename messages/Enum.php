<?php

/**
 * This is the common base class of all php language enumeration types.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package utils;
 * @subpackage class;
 * @since PHP 5.1;
 * @date April 2007;
 */
abstract class Enum {
	
	/**
	 * @var string variable to store value of the enum
	 */
	private $name = null;
	
	/**
	 * Sole constructor. Programmers cannot invoke this constructor. It is for use by singleton pattern instantiation in response to enum type declarations.
	 *
	 * @param string $name value of the element of the enumeration
	 */
	protected function __construct($name) {
		$this->name = $name;
	}
	
	/**
	 * Method to be invoked by subclasses extending enum to create a new vaue for the enumeration
	 *
	 * @param string $class enum class to instance
	 * @param string $var value of the element of the enumeration
	 */
	protected static function init($class, $name) {
		$enum = new $class();
		$enum->name = $name;
		return $enum;
	}

	/**
	 * Returns the name of this enum constant, as contained in the declaration.
	 * 
	 * @return string
	 */
	public function __toString() {
		return $this->name;
	}
	
	/**
	 * Returns the name of this enum constant, exactly as declared in its enum declaration.
	 * 
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

}
?>
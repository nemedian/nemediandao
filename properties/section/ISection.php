<?php
/**
 * Internal Section interface that you don't need to know about.
 */
interface ISection {

  /**
   * The implementation of this method must return the string representation of the section.
   *
   * @return string
   */
  public function toString();

}
/****************************** End of interface Properties_ISection ******************************/

?>
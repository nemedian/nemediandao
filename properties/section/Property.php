<?php

require_once(realpath(dirname(__FILE__) . '/ISection.php'));

/**
 * Internal Property section class that you don't need to know about.
 *
 * Everything in a properties file besides comments and blank lines, are considered as (name-value) properties.
 */
class Property implements ISection {

  private $key   = null;
  private $value = null;
  private $seperator = null;
  const SEPERATOR_REGEX_PATTERN = '[\\t \\x0c]*[=:\\t \\x0c][\\t \\x0c]*';

  /**
   * Overloaded constructor.
   *
   * Overload 1:
   * @param line a raw property line.
   *
   * Overload 2:
   * @param key
   * @param value
   * @param seperator optional seperator character (default '=').
   * @throw InvalidArgumentException
   */
  public function __construct() {
    $key = null;
    $value = null;
    $seperator = null;
    switch (func_num_args()) {
    case 1:
      $line = func_get_arg(0);
      if (isset($line)) {
        $line = ltrim($line, Properties::WHITE_SPACE_CHARS);
      }
      if (!(isset($line) && strlen($line))) {
      	throw new InvalidArgumentException('Empty line passed into overloaded constructor.');
      }
      $parts = $this->_parseLine($line);
      if (!$parts) {
        throw new InvalidArgumentException('Invalid property line passed into overloaded constructor.');
      }
      list($key, $seperator, $value) = $parts;
      break;
    case 2:
    case 3:
      $key   = func_get_arg(0);
      $value = func_get_arg(1);
      if (func_num_args() == 3) {
        $seperator = func_get_arg(2);
      }
      if (!isset($seperator)) {
        $seperator = '=';
      }
      break;
    default:
      throw new InvalidArgumentException('Invalid arguments passed into overloaded constructor.');
    }
    $this->_testKey($key);
    $this->_testValue($value);
    $this->_testSeperator($seperator);
    $this->key       = $key;
    $this->seperator = $seperator;
    $this->value     = $value;
  }


  /**
   * Splits a raw key-value line into it's constituent parts.
   * Returns an array with the 3 elements: key, seperator, value
   *
   * @param line reference
   * @return array
   */
  private static function _parseLine(&$line) {
    // Locate unescaped seperator.
    // Seperators match this sequence and may not be prefixed with an escape character: /\s*(=|:)\s*/
    $result = false;
    $key = $seperator = $value = null;
    $offset = 0;
    while (preg_match('/^((?U).+)(' . self::SEPERATOR_REGEX_PATTERN . ')(.*)$/s', substr($line, $offset), $matches, PREG_OFFSET_CAPTURE)) { // (?U) means ungreedy
      $possible_key = $offset ? substr($line, 0, $offset) . $matches[1][0] : $matches[1][0];
      $count = 0;
      $len = strlen($possible_key);
      for ($i = $len - 1; $i >= 0; $i--) {
        if (substr($possible_key, $i, 1) == '\\') {
          $count++;
        }
        else {
          break;
        }
      }
      if ($count % 2 == 0) { // even number of direct prior escapes is ok
      	$key       = $possible_key;
      	$seperator = $matches[2][0];
      	$value     = $matches[3][0];
      	break;
      }
      $offset += $matches[2][1];
    }
    if (!isset($key)) {
      $key = $line;
      $seperator = '=';
    }
    if (!isset($value)) {
      $value = '';
    }
    return array(self::unescape($key), $seperator, self::unescape($value));
  }


  /**
   * Tests an unescaped key's syntax. Throws an exception on error.
   *
   * @param key
   * @throw InvalidArgumentException
   */
  protected static function _testKey($key) {
    if (!isset($key)) {
      throw new InvalidArgumentException("Property key may not be NULL!");
    }
    if (!strlen($key)) {
      throw new InvalidArgumentException("Property key may not be empty!");
    }
  }


  /**
   * Tests an unescaped value's syntax. Throws an exception on error.
   *
   * @param value
   * @throw InvalidArgumentException
   */
  protected static function _testValue($value) {
    if (!isset($value)) {
      throw new InvalidArgumentException("Property value may not be NULL!");
    }
  }


  /**
   * Tests a seperator's syntax. Throws an exception on error.
   *
   * @param seperator   
   * @throw InvalidArgumentException
   */
  protected static function _testSeperator($seperator) {
    if (!isset($seperator)) {
      throw new InvalidArgumentException("Seperator may not be NULL!");
    }
    if (!strlen($seperator)) {
      throw new InvalidArgumentException("Seperator may not be empty!");
    }
    if (!preg_match('/^' . self::SEPERATOR_REGEX_PATTERN . '$/', $seperator)) {
      throw new InvalidArgumentException("Bad syntax in seperator string!");
    }
  }


  /**
   * Escapes a character.
   * The characters LF (line feed), CR (carriage return), FF (form feed), : (colon), # (hash), ! (bang),
   * = (equals), tab, and backslash are escaped C-style with a backslash character.
   * Characters outside the range 0x20 to 0x7E are encoded in the \\uxxxx format.
   * If $escape_space is true then the space character is escaped too.
   *
   * @param char character
   * @param escape_space boolean
   * @return string
   */
  protected static function _escapeChar($char, $escape_space) {
    static $escmap = null;
    if (is_null($escmap)) {
      $escmap = array("\r"      => 'r',
                      "\n"      => 'n',
                      chr(0x0c) => 'f',
                      "\t"      => 't',
                      '\\'      => '\\',
                      ':'       => ':',
                      '#'       => '#',
                      '!'       => '!',
                      '='	=> '=');
    }
    if (array_key_exists($char, $escmap)) {
      return '\\' . $escmap[$char];
    }
    elseif ($escape_space && ($char == ' ')) {
      return '\\ ';
    }
    elseif ((ord($char) < 0x20) || (ord($char) > 0x7e)) {
      return sprintf('\\u%04X', ord($char));
    }
    return $char;
  }


  /**
   * Escapes a string.
   * The characters LF (line feed), CR (carriage return), FF (form feed), : (colon), # (hash), ! (bang),
   * = (equals), tab, and backslash are escaped C-style with a backslash character.   
   * Characters outside the range 0x20 to 0x7E are encoded in the \\uxxxx format.
   * If $escapeSpace is true then the ' ' characters are escaped too.
   *
   * @param s
   * @param escape_space boolean
   * @return string
   */
  protected static function escape($s, $escape_space) {
    $result = preg_replace('/([\\s=:#!\\\\]|[^\\x20-\\x7e])/e', 'self::_escapeChar("\\1", $escape_space)', $s);
    // If the first character of a string is a space then always escape it, even if $escape_space is false.
    if (!$escape_space && strlen($result)) {
      if (substr($result,0,1) === ' ') {
        $result = "\\$result";
      }
    }
    return $result;
  }


  /**
   * Escapes a key.
   * The characters space, LF (line feed), CR (carriage return), FF (form feed), : (colon), # (hash), ! (bang),
   * = (equals), tab, and backslash are escaped C-style with a backslash character.   
   * Characters outside the range 0x20 to 0x7E are encoded in the \\uxxxx format.
   *
   * @param key
   * @return string
   */
  public static function escapeKey($key) {
    return self::escape($key, true);
  }


  /**
   * Escapes a value.
   * The characters LF (line feed), CR (carriage return), FF (form feed), : (colon), # (hash), ! (bang),
   * = (equals), tab, and backslash are escaped C-style with a backslash character.   
   * Characters outside the range 0x20 to 0x7E are encoded in the \\uxxxx format.
   * If the value starts with a whitespace character, then that too is escaped.
   *
   * @param value
   * @return string
   */
  public static function escapeValue($value) {
    return self::escape($value, false);
  }


  /**
   * Unescapes an escaped character sequence.
   *
   * @param s string, or array of matches from preg_replace_callback().
   * @return string
   */
  protected static function _unescapeChar($s) {
    if (is_array($s)) {
      $s = $s[0];
    }
    if (substr($s,0,1) != '\\') {
      return $s; // it wasn't escaped.
    }
    $s = substr($s,1); // drop \ character
    if (strlen($s) > 1) {
      if (!preg_match('/^u[\da-fA-F]{4}$/', $s)) {
        throw new Exception("Malformed \\uxxxx encoding in '\\$s'.");
      }
      $ord = hexdec(substr($s,1));      
      if ($ord < 128) { 
        return chr($ord); 
      } 
      else if ($ord < 2048) { 
        return (chr(192 + (($ord - ($ord % 64)) / 64))) .
               (chr(128 + ($ord % 64))); 
      }
      else { 
        return (chr(224 + (($ord - ($ord % 4096)) / 4096))) .
               (chr(128 + ((($ord % 4096) - ($ord % 64)) / 64))) .
               (chr(128 + ($ord % 64))); 
      } 
    }
    static $unescmap = null;
    if (is_null($unescmap)) {
      $unescmap = array('r'  => "\r",
                        'n'  => "\n",
                        'f'  => chr(0x0c),
                        't'  => "\t");
    }
    if (array_key_exists($s, $unescmap)) {
      return $unescmap[$s];
    }
    return $s;
  }


  /**
   * Unescapes a string.
   *
   * @param s
   * @return string
   */
  public static function unescape($s) {
    return preg_replace_callback('/(\\\\u[\da-fA-F]{4}|\\\\(.))/', array(__CLASS__, '_unescapeChar'), $s);
  }


  /**
   * Returns the string representation of the section.
   *
   * @param max_chars_per_line unsigned integer, maximum number of characters per line (default 120).
   * @return string
   */
  public function toString($max_chars_per_line = 120) {
    if (isset($max_chars_per_line)) {
      if ($max_chars_per_line < 1) {
        $max_chars_per_line = null;
      }
    }
    $key_and_sep = $this->escapeKey($this->key) . $this->seperator;
    $value = $this->escapeValue($this->value);
    if (!$max_chars_per_line || (strlen($key_and_sep) + strlen($value) <= $max_chars_per_line)) {
      return "$key_and_sep$value\n";
    }
    $lines = array();
    if (strlen($key_and_sep) > $max_chars_per_line / 2) { // key and seperator are quite long so leave them on their own line
      array_push($lines, $key_and_sep);
    }
    elseif (preg_match('/^.{1,' . ($max_chars_per_line - strlen($key_and_sep)) . '}(?<!\\\\)[ \\t]/', $value, $matches)) { // if first breakable part of value is short then place it on same line as key   	
      if (strlen($key_and_sep) < 8) { // common tab length
      	array_push($lines, $key_and_sep . "\t" . $matches[0]);
      }
      else {
        array_push($lines, $key_and_sep . $matches[0]);
      }      
      $value = substr($value, strlen($matches[0]));
    }
    else {
      array_push($lines, $key_and_sep); // because value seems too unbreakable to put on same line as key.
    }       
    // split out the (rest of) value
    $parts = preg_split('/(.{1,' . $max_chars_per_line . '}(?<!\\\\)[ \\t])/', $value, -1, PREG_SPLIT_DELIM_CAPTURE);    
    for ($i = 0; $i < count($parts); $i += 2) {
      $len = strlen($parts[$i]); // usually empty, unless unsplitable.
      if ($len > $max_chars_per_line) {
        $line = '';
        for ($j = 0; $j < $len; $j += $max_chars_per_line) {       	
          $line .= substr($parts[$i], $j, $max_chars_per_line);
          if ($j + $max_chars_per_line < $len - 1) {
            $line .= "\\\n\t";
          }
        }
      }
      else {
        $line = $parts[$i];
      }
      if ($i+1 < count($parts)) {
      	if (strlen($line)) {
      	  $line .= "\\\n\t";
        }
        $line .= $parts[$i+1]; // the preg_split delimiter
      }
      array_push($lines, $line);
    }    
    return implode("\\\n\t", $lines) . "\n";
  }


  /**
   * Sets the value.
   *
   * @param value
   */
  public function setValue($value) {
    if ((string)$value != $this->value) {
      $this->testValue($value);
      $this->value = (string) $value;
    }
  }


  /**
   * Returns the key.
   *
   * @return string
   */
  public function getKey() {
    return $this->key;
  }


  /**
   * Returns the value.
   *
   * @return string
   */
  public function getValue() {
    return $this->value;
  }

}
/****************************** End of class Properties_Section_Property ******************************/

?>
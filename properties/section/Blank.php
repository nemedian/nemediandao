<?php


require_once(realpath(dirname(__FILE__) . '/ISection.php'));

/**
 * Internal Blank section class that you don't need to know about.
 * Lines that contain nothing or only whitespace are considered blanks.
 */
class Blank implements ISection {

  /**
   * Returns the string representation of the section.
   *
   * @return string
   */
  public function toString() {
    return "\n";
  }

}
/****************************** End of class Properties_Section_Blank ******************************/

?>
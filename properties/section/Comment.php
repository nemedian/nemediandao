<?php

require_once(realpath(dirname(__FILE__) . '/ISection.php'));

/**
 * Internal Comment section class that you don't need to know about.
 *
 * A comment line is a line whose first non-whitespace character
 * is an ASCII # or ! is ignored (thus, # (hash) or ! (bang) indicate comment lines).
 */
class Comment implements ISection {

  private $comment_char = null;
  private $padding      = null;
  private $value        = null;

  /**
   * Constructor.
   *
   * @param value        string
   * @param comment_char optional comment character, default '#' (allowed values are '#' or !').
   * @param padding      optional comment padding string, default ' '.
   * @throw InvalidArgumentException
   */
  public function __construct($value, $comment_char = '#', $padding = ' ') {
    $this->value = $value;
    $this->_testCommentChar($comment_char);
    $this->comment_char = $comment_char;
    $this->_testPadding($padding);
    $this->padding = $padding;
  }


  /**
   * Tests the syntax of a comment character.
   *
   * @param $char
   * @throw InvalidArgumentException
   */
  protected static function _testCommentChar($char) {
    if (!isset($char)) {
      throw new InvalidArgumentException('Comment character may not be null.');
    }
    if (($char != '#') && ($char != '!')) {
      throw new InvalidArgumentException('String "' . $char . '" is not a valid comment character.');
    }
  }


  /**
   * Tests the syntax of a padding string.
   *
   * @param string
   * @throw InvalidArgumentException
   */
  protected static function _testPadding($string) {
    if (!isset($string)) {
      throw new InvalidArgumentException("Padding may not be NULL!");
    }
    if (!preg_match('/^( |\\t|#|!)*$/', $string)) {
      throw new InvalidArgumentException('Padding string contains invalid characters.');
    }
  }


  /**
   * Returns the string representation of the section.
   *
   * @return string
   */
  public function toString() {
    $lines = preg_split('/\\r?\\n\\r?/', $this->value);
    return $this->comment_char . $this->padding . implode("\n" . $this->comment_char . $this->padding, $lines) . "\n";
  }


  /**
   * Sets the value. This is the complete comment, but without the initial comment character and padding.
   *
   * @param value string
   */
  public function setValue($value) {
    $this->value = $value;
  }


  /**
   * Returns the value. This is the complete comment, but without the comment character and padding.
   *
   * @return string
   */
  public function getValue() {
    return $this->value;
  }

}
/****************************** End of class Properties_Section_Comment ******************************/

?>
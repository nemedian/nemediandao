<?php

require_once 'log4php/LoggerManager.php';
require_once 'db/mysqli/MysqliQueryRunner.php';
require_once 'db/mysqli/MysqliConnection.php';
require_once 'db/interfaces.php';

/**
 * Represent a transaction manager for a MysqliConnection;
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package db;
 * @subpackage mysqli;
 * @since PHP 5.1;
 * @see TransactionManager
 * @date April 2007;
 */
class MysqliTransactionManager implements TransactionManager {
	
	/**
	 * @var QueryRunner object to make data operations
	 */
	private $queryRunner;
	
	/**
	 * @var Mysqli it's the php built-in class to manage data for mysqli
	 */
	private $dataHandler;
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * @var MysqliConnection: connection of type mysqli;
	 */
	private $connection;
	
	/**
	 * @var bool: true if transaction has been rollback
	 */
	private $rollback = false;
	
	/**
	 * Constructor: creates a new transaction manager
	 *
	 * @param MysqliDataSource $connection: connection transaction manager operates on
	 * @param Mysqli $dataHandler: php built-in class to manage data for mysqli
	 */
	public function __construct(MysqliConnection $connection, Mysqli $dataHandler) {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->connection = $connection;
		$this->dataHandler = $dataHandler;
		$this->queryRunner = new MysqliQueryRunner($dataHandler);
	}
	
	/**
	 * Start a new transaction
	 *
	 * @see TransactionManager::startTransaction()
	 */
	function startTransaction() {
		$this->dataHandler->autocommit(false);
			
		// reinit rollback variable;
		$this->rollback = false;
		
		// increase transaction stack on connection
		$this->connection->addTransactionStack();
	}
	
	/**
	 * Commit active transaction
	 *
	 * @see TransactionManager::commit()
	 */
	public function commit() {
		
		// decrease transaction stack on connection
		if ($this->connection->subTransactionStack() == 0) {
			$this->dataHandler->commit();
			self::$logger->debug("Commit executed");
		}
	}
	
	/**
	 * Rollback active transaction;
	 *
	 * @see TransactionManager::rollback()
	 */
	public function rollback() {
		
		if ($this->rollback == false) {
			$this->dataHandler->rollback();
			
			// reinit transaction stack on connection();
			$this->connection->initTransactionStack();

			$this->rollback = true;
			self::$logger->debug("Rollback executed");
			
		}
		
	}
	
	/**
	 * Return the queryRunner object to execute operations on data inside active transaction
	 *
	 * @return QueryRunner: object to execute data operations
	 * @see TransactionManager::getQueryRunner()
	 */
	public function getQueryRunner() {
		return $this->queryRunner;
	}

}
?>
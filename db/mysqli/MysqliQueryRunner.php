<?php

require_once 'log4php/LoggerManager.php';
require_once 'db/DatabaseException.php';
require_once 'db/interfaces.php';

/**
 * Object to manage data; It automatically manages binding of parameters, executing of statements, result handling;
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package db;
 * @subpackage mysqli;
 * @since PHP 5.1;
 * @see MysqliQueryRunner;
 * @date April 2008;
 */
class MysqliQueryRunner implements QueryRunner {
	
	/**
	 * @var string corresponding variable has type integer
	 */
	private static $INTEGER_PARAMETER = "i";
	
	/**
	 * @var string corresponding variable has type double
	 */
	private static $DOUBLE_PARAMETER = "d";
	
	/**
	 * @var string corresponding variable has type string
	 */
	private static $STRING_PARAMETER = "s";
	
	/**
	 * @var string: corresponding variable is a blob and will be send in packets
	 */
	private static $BLOB_PARAMETER = "b";
	
	/**
	 * @var Mysqli it's the php built-in class to manage data for mysqli
	 */
	private $dataHandler;
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * Constructor: creates a new query runner
	 *
	 * @param Mysqli $dataHandler php built-in class to manage data for mysqli
	 */
	public function __construct(Mysqli $dataHandler) {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->dataHandler = $dataHandler;
	}
	
	/**
	 * Performs a query on data source; some result (even an empty one) is expected;
	 * 
	 * @param string $stmt it represent the query to perform;
	 * @param array $args arguments for the query to perform;
	 * @return array of associative arrays in which keys are alias defined in statement and values
	 * are data values from datasource;
	 * @throws DatabaseException raised if an error occurs;
	 * @see QueryRunner::query()
	 */
	public function query($stmt, array $args = array()) {
		
		try {
			
			// execute statement
			$result = $this->executeStatement($stmt, $args);
			
			// fetch results
			$toReturn = $this->fetch($result);
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new DatabaseException("Execution of prepared statement failed");
		}
		
		return $toReturn;
	}
	
	/**
	 * Performs a statement execution on data source; result is expected; method returns last inserted id;
	 * If $args is a multidimensional array, multiple query are performed, using the same statement
	 * with the different given set of args.
	 * 
	 * @param string $stmt it represent the query to perform;
	 * @param array $args arguments for the query to perform;
	 * @return mixed last inserted id (int) if a single insert is executed, an array with inserted ids if multiple insert are executed.
	 * @throws DatabaseException raised if an error occurs;
	 * @see QueryRunner::execute();
	 */
	public function execute($stmt, array $args = array()) {
		
		try {
			
			// execute statement
			$this->executeStatement($stmt, $args);
			
			// return inserted ids
			return (is_array(current($args)) && count(current($args)) > 0 && $this->dataHandler->insert_ids != null) ? $this->dataHandler->insert_ids : $this->dataHandler->insert_id;
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new DatabaseException("Execution of prepared statement failed");
		}
		
		return null;
	}
	
	/**
	 * Initializes and prepares the statement; does the params binding then execute the statement and return result;
	 * If $args is a multidimensional array, multiple query are performed, using the same statement
	 * with the different given set of args.
	 * 
	 * @param string $stmt statement to execute
	 * @param array $args parameters execute binding to
	 * @return mysqli_stmt data from datasource
	 */
	private function executeStatement($stmt, array $args = array()) {
		
		try {
			
			// statement to execute
			self::$logger->debug("Executing statement: " . $stmt);
			
			// initialize statement
			$preparedStmt = $this->dataHandler->stmt_init();
			
			// prepare statement
			$preparedStmt->prepare($stmt);
			
			$parameters = array();
			
			$binding = null;
			$types = null;
			
			if (count($args) > 0) {
				
				if (is_array(current($args)) && count(current($args)) > 0) {
					// if is array multiple execution will be performed
					$parameters = $args;
				} else {
					array_push($parameters, $args);
				}
				
				// compose binding command
				$numParam = count(current($parameters));
				
				$types = $this->retrieveTypes(current($parameters));
				
				$binding = '$preparedStmt->bind_param(\'' . $types . '\'';
				for ($i = 0; $i < $numParam; $i++) {
					$binding .= ', $param[' . $i . ']';
				}
				$binding .= ');';
			
			}
			
			$this->dataHandler->insert_ids = array();
			
			do {
				
				$param = null;
				
				if (count($parameters) > 0) {
					$param = array_pop($parameters);
					eval($binding);
				}
				
				// execute statement
				$preparedStmt->execute();
				$this->dataHandler->insert_ids[] = $this->dataHandler->insert_id;
				
				// log execution
				$log = "Statement executed.";
				if ($param != null) {
					$log .= " Args: [" . implode(', ', $param) . "]";
				}
				
				self::$logger->debug($log);
				
				if ($preparedStmt->error != null) {
					throw new DatabaseException($preparedStmt->error);
				}
			
			} while (count($parameters) > 0);
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			if ($this->dataHandler->error != null) {
				self::$logger->error($this->dataHandler->error);
			}
			throw new DatabaseException("Prepare of prepared statement failed");
		}
		
		return $preparedStmt;
	}
	
	/**
	 * fetch results
	 * 
	 * @param mysqli_stmt $result object containing results for mysqli query
	 * @return array contains associative arrays with results;
	 */
	private function fetch(mysqli_stmt $result) {
		
		static $BIND_RESULT_FUNC = "mysqli_stmt_bind_result";
		
		// extract metadata
		$data = $result->result_metadata();
		
		// First value has to be a reference to the stmt.
		// Because mysqli_stmt_bind_result requires the link to $stmt as the first param.
		$fieldValue[0] = $result;
		
		$array = array();
		
		//load the name of field result
		while ($field = mysqli_fetch_field($data)) {
			$fieldValue[$field->name] = &$array[$field->name];
		}
		
		// bind variable
		call_user_func_array($BIND_RESULT_FUNC, $fieldValue);
		
		$toReturn = array();
		
		// retrieve field names
		$keys = array_keys($fieldValue);
		
		// remove statement key
		unset($keys[0]);
		
		// fetch result into an array
		while (mysqli_stmt_fetch($result)) {
			
			foreach ($keys as $key) {
				$element[$key] = $fieldValue[$key];
			}
			
			array_push($toReturn, $element);
		
		}
		
		return $toReturn;
	
	}
	
	/**
	 * Retrieve data types of the arguments passed in the array. It compose a string as result, each char of the string
	 * represent the type of the correnspondent object in the array of args
	 * 
	 * @param array $args arguments you want retrieve type
	 * @return string each char of the string represent the type of the correnspondent object in the array of args
	 * @example if arguments are $args = array( 1, "Hello World!", true): string result is "isi"
	 */
	private function retrieveTypes(array $args) {
		$toReturn = "";
		
		foreach ($args as $arg) {
			if (is_string($arg)) {
				$toReturn .= self::$STRING_PARAMETER;
			} else if (is_int($arg)) {
				$toReturn .= self::$INTEGER_PARAMETER;
			} else if (is_float($arg)) {
				$toReturn .= self::$DOUBLE_PARAMETER;
			} else if (is_bool($arg)) {
				$toReturn .= self::$INTEGER_PARAMETER;
			} else if (is_null($arg)) {
				$toReturn .= self::$INTEGER_PARAMETER;
			} else {
				self::$logger->error("Query parameters type not correct");
			}
		}
		
		return $toReturn;
	}

}

?>
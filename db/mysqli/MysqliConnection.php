<?php

require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/DatabaseException.php';
require_once 'db/mysqli/MysqliTransactionManager.php';
require_once 'db/mysqli/MysqliDataSource.php';

/**
 * Represent a connection to a MysqliDataSource;
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package db;
 * @subpackage mysqli;
 * @since PHP 5.1;
 * @see Connection
 * @date April 2008;
 */
class MysqliConnection implements Connection {
	
	/**
	 * Each connection can have only one active transaction; if a second transaction is requested before
	 * the first one is terminated (before commit or rollback is called), the new transaction is not a really new
	 * one but its operations are executed inside the first one;
	 * So, a begin transaction start a new transaction only if no active transactions are already started.
	 * 
	 * @var int: number of nested transactions collapsed in the actual active transaction;
	 */
	private $transactionStack = 0;
	
	/**
	 * @var LoggerCategory: logger for class;
	 */
	private static $logger;
	
	/**
	 * @var Mysqli: php built-in class to manage data for mysqli
	 */
	private $dataHandler;
	
	/**
	 * @var MysqliTransactionManager: it's the manager for transactions for this connection
	 */
	private $transactionManager;
	
	/**
	 * @var bool: true if this connection to datasource is closed;
	 */
	private $closed;
	
	/**
	 * Constructor: creates a new connection to given MysqliDataSource;
	 *
	 * @param MysqliDataSource $dataSource data source to connect
	 * @throws DatabaseException raised if an error occurs;
	 */
	public function __construct(MysqliDataSource $dataSource) {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$host = $dataSource->getHost();
		$port = $dataSource->getPort();
		$dbName = $dataSource->getDbName();
		$user = $dataSource->getUser();
		$passwd = $dataSource->getPasswd();
		
		// physically connect to data source
		$this->connect($host, $user, $passwd, $dbName, $port);
		
		// create a new transaction manager for this connection
		$this->transactionManager = new MysqliTransactionManager($this, $this->dataHandler);
	}
	
	/**
	 * Destruct connection object and physically close the connection;
	 */
	public function __destruct() {
		$this->close();
	}
	
	/**
	 * physically connect to DataSource using given parameters
	 *
	 * @param string $host host where database is
	 * @param string $port port where database is
	 * @param string $dbName schema name of database
	 * @param string $user user to access database
	 * @param string $passwd password to access database
	 * @throws DatabaseException raised if an error occurs;
	 */
	private function connect($host, $user, $passwd, $dbName, $port) {
		try {
			$this->dataHandler = new Mysqli($host, $user, $passwd, $dbName, $port);
		} catch (Exception $e) {
			self::$logger->debug($e->getMessage());
			throw new DatabaseException($e->getMessage());
		}
		self::$logger->debug("Connection to DataSource opened");
	}
	
	/**
	 * Return the transaction manager for this connection
	 *
	 * @return MysqliTransactionManager transaction manager for this connection
	 * @see Connection::getTransactionManager()
	 */
	public function getTransactionManager() {
		return $this->transactionManager;
	}
	
	/**
	 * Return if the connection to data source is closed
	 *
	 * @return bool true if connection is closed
	 * @see Connection::isClosed()
	 */
	public function isClosed() {
		return $this->closed;
	}
	
	/**
	 * Physically close the connection to data source
	 *
	 * @see Connection::close()
	 * @throws DatabaseException raised if an error occurs;
	 */
	public function close() {
		try {
			if (! $this->closed) {
				$this->closed = $this->dataHandler->close();
				self::$logger->debug("Connection to DataSource closed");
			}
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new DatabaseException($e->getMessage());
		}
	}
	
	/**
	 * Method to manage transaction; This method like other for transactions are special for MysqliConnection;
	 * Increase transaction stack by one;
	 * 
	 * @see $transactionStack;
	 */
	public function addTransactionStack() {
		$num = $this->transactionStack + 1;
		self::$logger->debug("Increase transactionStack to: " . $num);
		return $this->transactionStack++;
	}
	
	/**
	 * Method to manage transaction; This method like other for transactions are special for MysqliConnection;
	 * Decrease transaction stack by one;
	 * 
	 * @see $transactionStack;
	 */
	public function subTransactionStack() {
		$num = $this->transactionStack - 1;
		self::$logger->debug('Decrease transactionStack to: ' . $num);
		
		// check that transaction stack is not less than 0
		$this->transactionStack -= 1;
		if ($this->transactionStack < 0) {
			$this->transactionStack = 0;
			$message = "Trying to commit more transaction then started.";
			self::$logger->warn($message);
		}
		
		return $this->transactionStack;
	}
	
	/**
	 * Method to manage transaction; This method like other for transactions are special for MysqliConnection;
	 * Init transaction stack setting value to 0;
	 * 
	 * @see $transactionStack;
	 */
	public function initTransactionStack() {
		self::$logger->debug('Init transactionStack');
		$this->transactionStack = 0;
	}

}
?>
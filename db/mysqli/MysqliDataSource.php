<?php

require_once 'db/interfaces.php';
require_once 'db/mysqli/MysqliConnection.php';

/**
 * Represent the datasource for Mysqli
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package db;
 * @subpackage mysqli;
 * @since PHP 5.1;
 * @see DataSource
 * @date April 2008;
 */
class MysqliDataSource implements DataSource {

	/**
     * @var string host where database is
     */
	private $host;
	
	/**
     * @var string port where database is
     */
	private $port;
	
	/**
     * @var string schema name of database
     */
	private $dbName;
	
	/**
     * @var string user to access database
     */
	private $user;
	
	/**
     * @var string password to access database
     */
	private $passwd;
	
	/**
     * Creates a MysqliDataSource with give parameters;
     *
     * @param string $host host where database is
     * @param string $port port where database is
     * @param string $dbName schema name of database
     * @param string $user user to access database
     * @param string $passwd password to access database
     */
	public function __construct($host, $port, $dbName, $user, $passwd){
		$this->host = $host;
		$this->port = $port;
		$this->dbName = $dbName;
		$this->user = $user;
		$this->passwd = $passwd;
	}
	
	/**
     * Return a new connection to MysqliDataSource;
     *
     * @return MysqliConnection new connection to datasource
     * @see DataSource::createConnection()
     */
	public function createConnection(){
		return new MysqliConnection($this);
	}
		
	/**
     * Return host where database is;
     *
     * @return string host where database is;
     */
	public function getHost(){
		return $this->host;
	}
			
	/**
     * Return port where database is;
     *
     * @return string port where database is;
     */
	public function getPort(){
		return $this->port;
	}
			
	/**
     * Return database schema;
     *
     * @return string database schema;
     */
	public function getDbName(){
		return $this->dbName;
	}
			
	/**
     * Return user to access database;
     *
     * @return string user to access database;
     */
	public function getUser(){
		return $this->user;
	}

	/**
     * Return password to access database;
     *
     * @return string password to access database;
     */
	public function getPasswd(){
		return $this->passwd;
	}

}
?>
<?php

require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/DaoManager.php';
require_once 'db/DaoException.php';
 
/**
 * Data Access Object, provides standards methods to manage access and mapping results;
 * It can manage statement retrieval, query execution, object mapping.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package common;
 * @subpackage dao;
 * @since PHP 5.1;
 * @see IDao
 * @date December 2008;
 */
abstract class Dao {
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * @var DaoManager to manage nested queries;
	 */
	private static $daoManager;
	
	/**
	 * @var array associative array collecting and caching statements;
	 * It contains for each path an associative array composed by statementId -> statement
	 */
	private static $statements = array();
	
	/**
	 * @var array associative array collecting class statements paths (sources)
	 */
	private $paths = array();
	
	/**
	 * @var QueryRunner object to physical delegate execution of queries for DataSource;
	 */
	private $queryRunner;
	
	/**
	 * Creates a DataAccess Object based on given queryRunner;
	 *
	 * @param QueryRunner $queryRunner DataSource manager to delegate any data operation (statement execution)
	 */
	public function __construct(QueryRunner $queryRunner) {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->queryRunner = $queryRunner;
	}
	
	/**
	 * Retrieve the proxy Data Access Object associated to given interface;
	 * Method is for Dao internal usage (nested queries);
	 *
	 * @param string iface represents the name of interface required;
	 * @return DaoProxy associated to given interface.
	 */
	protected function getDao($iface) {
		if (self::$daoManager == null) {
			self::$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);
		}
		return self::$daoManager->getDao($iface);
	}
	
	/**
	 * Execute query with given arguments and return a single result;
	 * This method must be used only if a single result is expected; If query returns multiple result
	 * an error occurred and an exception is raised;
	 *
	 * @param string $statement query to execute;
	 * @param array $args argument expected for query variable bindings. If not arguments are needed, param is not required;
	 * @param string $class represents the class to map the result. If class this param not provided, result is mapped into an associative array;
	 * @return array associative array, with keys equals to selected fields names, is returned if the case $class param is not provided;
	 * @return object if class $class is provided, an object s dynamically instantiated for that class and result is mapped into that object;
	 * @throws DaoException raised if an error occurs;
	 */
	protected function queryForObject($statement, array $args = array(), $class = null) {
		
		// execution of the statement
		self::$logger->debug("Query to execute: " . $statement);
		
		try {
			$result = $this->queryRunner->query($statement, $args);
			
			// checking number of results retrieved
			if (count($result) == 0) {
				self::$logger->debug("No value returned");
				return null;
			
			} elseif (count($result) == 1) {
				
				// if param class is specified result object is instanziated and result is mapped into
				if ($class != null) {
					$toReturn = $this->map2object($result[0], $class);
				} else {
					$toReturn = $result[0];
				}
				
				return $toReturn;
			
			} elseif (count($result) > 1) {
				$message = "Query for object can not returned a multiple result: " . LOG4PHP_LINE_SEP . $statement . LOG4PHP_LINE_SEP . "; Return class: $class";
				self::$logger->warn($message);
				throw new DaoException($message);
			}
		
		} catch (DaoException $e) {
			throw $e;
		
		} catch (Exception $e) {
			$message = "Query execution failed. Sql to execute: " . LOG4PHP_LINE_SEP . $statement . LOG4PHP_LINE_SEP . " Return class: $class";
			self::$logger->error($message);
			throw new DaoException($message);
		}
		
		// this code imust be never reached
		return null;
	}
	
	/**
	 * Execute query with given arguments and return a list of result mapped into an array;
	 *
	 * @param string $statement query to execute;
	 * @param array $args argument expected for query variable bindings. If not arguments are needed, param is not required;
	 * @param string $class represents the class to map the result. If class this param not provided, result is mapped into an associative array;
	 * @return array array of associative arrays, with keys equals to selected fields names, is returned if the case $class param is not provided;
	 * 				  Else if class $class is provided, an array of objects, dynamically instantiated, is returned;
	 * @throws DaoException raised if an error occurs;
	 */
	protected function queryForList($statement, array $args = array(), $class = null) {
		
		// execution of the statement
		self::$logger->debug("Query to execute: " . $statement);
		
		try {
			// execute query
			$result = $this->queryRunner->query($statement, $args);
			
			$toReturn = array();
			
			if ($class != null) {
				
				// if param class is specified result object is istanzited and result is mapped into
				foreach ($result as $field) {
					array_push($toReturn, $this->map2object($field, $class));
				}
			
			} else {
				
				// simple array result
				$toReturn = $result;
			}
		
		} catch (Exception $e) {
			$message = "Query execution failed. Sql to execute: " . LOG4PHP_LINE_SEP . $statement . LOG4PHP_LINE_SEP . "Return class: $class";
			self::$logger->error($e->getMessage());
			throw new DaoException($message);
		}
		
		return $toReturn;
	
	}
	
	/**
	 * Execute a statement with given arguments; No result is expected;
	 * If $args is a multidimensional array, multiple executions are performed, using the same statement
	 * with the different given set of args.
	 * 
	 * @param string $statement statement to execute;
	 * @param array $args argument expected for query variable bindings. If not arguments are needed, param is not required;
	 * 		Arguments could be an array of arrays, in that case batch execution of the prepared statement is performed by the QueryRunner;
	 * @throws DaoException raised if an error occurs;
	 */
	protected function execute($statement, array $args = array()) {
		
		// execution of the statement
		self::$logger->debug("Statement to execute: " . $statement);
		
		try {
			
			return $this->queryRunner->execute($statement, $args);
		
		} catch (Exception $e) {
			$message = "Statement execution failed. Sql to execute: " . LOG4PHP_LINE_SEP . $statement . LOG4PHP_LINE_SEP;
			self::$logger->error($e->getMessage());
			throw new DaoException($message);
		}
		
		return null;
	}
	
	/**
	 * Retrieve statement associated to $key passed;
	 *
	 * @param string $key key associated at the statement needed; it's searched in every saved path so must keep mind the order paths are saved;
	 * @return string: statement;
	 * @throws DaoException: raised if an error occurs;
	 */
	protected function getStatement($key) {
		
		try {
			
			$found = false;
			
			// searching statement key in every path
			foreach ($this->paths as $path) {
				if (array_key_exists($path, self::$statements) && array_key_exists($key, self::$statements[$path])) {
					
					$found = true;
					
					// statement is found
					self::$logger->debug("Statement found");
					return self::$statements[$path][$key];
				
				}
			}
			
			if (! $found) {
				throw new DaoException("Error in retrieving statement");
			}
		
		} catch (Exception $e) {
			$message = "Get statement with key = $key failed";
			self::$logger->error($e->getMessage());
			throw new DaoException($message);
		}
		
		return null;
	}
	
	/**
	 * Add a statement path (source) to array of path to be cheched during statement retrieving;
	 *
	 * @param string $path path do add;
	 */
	protected function addStatementsPath($path) {
		
		if (in_array($path, $this->paths) === false) {
			if (! array_key_exists($path, self::$statements)) {
				$this->parseXmlDocument($path);
			}
			$this->paths[] = $path;
		}
	
	}
	
	/**
	 * Map associative array into an object of class $class, dynamically instantiating it;
	 *
	 * @param array $map: associative array, with keys (equals to selected fields names) and values;
	 * @param string $class: class to instance and fill with data from $map;
	 * @return object: requested object;
	 */
	protected function map2object(array $map, $class) {
		
		static $SET = 'set';
		
		$object = Factory::newInstance($class);
		foreach ($map as $field => $value) {
		    $method = $SET . ucfirst($field);
			$object->$method($value);
		}
		return $object;
	}
	
	/**
	 * Parse an xml document read from given path, and put each read statement in the statement map for further access;
	 *
	 * @param string $path: path of the file containing statements;
	 */
	private function parseXmlDocument($path) {
		
		// xml constants
		static $STATEMENT = "statement";
		static $ID = "id";
		
		$doc = new DOMDocument();
		$doc->load($path);
		$statements = $doc->getElementsByTagName($STATEMENT);
		
		// parsing statements of path
		foreach ($statements as $statement) {
			
			self::$statements[$path][$statement->getAttribute($ID)] = $statement->nodeValue;
			self::$logger->debug("Statement cached: " . $path . " -> " . $statement->getAttribute($ID));
		
		}
	
	}

}
?>
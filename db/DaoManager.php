<?php

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoException.php';
require_once 'db/DaoProxy.php';
require_once 'db/interfaces.php';
require_once 'db/DaoLoader.php';
require_once 'db/Factory.php';

/**
 * Manager for Dao objects; this object structure is composed by an array of instances of DaoManager.
 * Each DaoManager instance is associated to a specified Dao config file and it's a singleton for that file.
 * So that's a map of singleton; It also is the connection manager for daos, silently connect to datasource where
 * required or reusing a connection where possible.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package common;
 * @subpackage dao;
 * @since PHP 5.1;
 * @date January 2008;
 */
class DaoManager {
	
	/**
	 * @var associative array containing each dao,
	 * its iface class name as key and its implementation full class name as value;
	 */
	private $daos;
	
	/**
	 * @var array associative array caching already requested DaoProxy
	 */
	private $proxedDaos = array();
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * @var DataSource of dao;
	 */
	private $dataSource;
	
	/**
	 * @var Connection to DataSource. It's null if no connection is present.
	 */
	private $connection;
	
	/**
	 * @var array. It contains instances of DaoManager class. That's an associative array, with keys
	 * representing dao config file and value representing dao manager for that file.
	 */
	private static $instances = array();
	
	/**
	 * Constructor: creates a DaoManager. It's private to implement the singletong pattern;
	 * 
	 * @param array $daos array containing for each dao iface name the implementation dao class full name to instance
	 * @param DataSource $dataSource data source to connect
	 */
	private function __construct(array $daos, DataSource $dataSource) {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->daos = $daos;
		$this->dataSource = $dataSource;
	}
	
	/**
	 * Starts a new Transaction if no Transactions are active at the time;
	 * Use this method if you want explicitly operate with transacltion.
	 * Il a transaction is started in this way it expected to be closed with the endTransaction method
	 * Anyway, standar transaction policy is applied
	 * 
	 * @see TransactionManager::startTransaction()
	 */
	public function startTransaction() {
		$this->connect();
		$this->connection->getTransactionManager()->startTransaction();
	}
	
	/**
	 * Commit the current transaction; Be carefull to use the commitTransaction method explicitly if
	 * a startTransaction method has benn previously called
	 * 
	 * @see TransactionManager::commit()
	 */
	public function commitTransaction() {
		if ($this->connection == null || $this->connection->isClosed()) {
			throw new DaoException("Not existing connection.");
		}
		$this->connection->getTransactionManager()->commit();
	}
	
	/**
	 * Rollback the current transaction; If no commit is called the rollback status of query is effectivly restored
	 * 
	 * @see TransactionManager::rollback()
	 */
	public function rollbackTransaction() {
		if ($this->connection == null || $this->connection->isClosed()) {
			throw new DaoException("Not existing connection.");
		}
		$this->connection->getTransactionManager()->rollback();
	}
	
	/**
	 * Static method that return the unique instance of DaoManager associated
	 * to given dao config file path; if the DaoManager for that file doesn't exists,
	 * an instance is automatically created and added in the DaoManager's associative array
	 *
	 * @param string $daoConfigPath: represent the dao config file path for which a DaoManager is requested;
	 * @return DaoManager associated to given dao config file path;
	 */
	public static function getInstance($daoConfigPath) {
		try {
			if (self::$logger == null) {
				self::$logger = & LoggerManager::getLogger(__CLASS__);
			}
			
			// check if an instance of DaoManager for given path already exists
			if (! array_key_exists($daoConfigPath, self::$instances)) {
				
				// register new instance in DaoManager instance arrays
				self::$instances[$daoConfigPath] = self::newInstance($daoConfigPath);
			}
		
		} catch (Exception $e) {

			self::$logger->error($e->getMessage());
			throw new DaoException("Error getting " . __CLASS__ . " instance associated to: $daoConfigPath");
		}
		//return instance requested
		return self::$instances[$daoConfigPath];
	}
	
	/**
	 * Static method that return a new instance of DaoManager associated
	 * to given dao config file path; his method is useful because physically each DaoManager
	 * is associated to a DataSource and in some cases we need fresh resources
	 * (for example a new database connections) for a DaoManager even if DataSource is the same.
	 *
	 * @param string $daoConfigPath: represent the dao config file path for wich a DaoManager is requested;
	 * @return DaoManager
	 */
	public static function newInstance($daoConfigPath) {
		try {
			
			if (self::$logger == null) {
				self::$logger = & LoggerManager::getLogger(__CLASS__);
			}
			// parse given dao config path
			$daoLoader = new DaoLoader($daoConfigPath);
		
            // set DaoManager properties
			$args = array();
			array_push($args, $daoLoader->getDaosMap());
			array_push($args, $daoLoader->getDataSource());
			
			// instantiate a new DaoManager
			return new self($daoLoader->getDaosMap(), $daoLoader->getDataSource());
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new DaoException("Error getting new " . __CLASS__ . " instance for: $daoConfigPath");
		}
		
		// never happens;
		return null;
		
	}
    
    /**
	 * Perform a connection to database if a dao instance is requested or if a transaction
	 * is explicitly started
	 */
	private function connect() {
        self::$logger->debug("connect");
			
		// get an usable connection for datasource
		if ($this->connection == null || $this->connection->isClosed()) {
			$this->connection = $this->dataSource->createConnection();
		}
	}
    
	/**
	 * Method that return a dao (wrapper in its daoProxy) given its interface;
	 *
	 * @param string $idao: it's the name of interface associated to a dao object;
	 * @return DaoProxy: proxy for dao requested;
	 */
	public function getDao($idao) {
		
		try {
			
			self::$logger->debug("Get implementation for interface: " . $idao);
			
			// get an usable connection for datasource
			$this->connect();
			
			$proxy = null;
			
			if (! array_key_exists($idao, $this->proxedDaos)) {
				
				// get transaction manager for that connection
				$transactionManager = $this->connection->getTransactionManager();
				
				// instantiate a dao object
				$dao = Factory::newInstance($this->daos[$idao]["implementation"], array($transactionManager->getQueryRunner()));
				
				// instance a new DaoProxy
				
                $proxy = new DaoProxy($dao, $transactionManager);
				if (isset($this->daos[$idao]["memoizedMethods"])) {
					$proxy->setMemoizedMethods($this->daos[$idao]["memoizedMethods"]);
				}
				
				// put DaoProxy in array of instanced DaoProxy
				$this->proxedDaos[$idao] = $proxy;
			
			} else {
				// get requested DaoProxy
				$proxy = $this->proxedDaos[$idao];
			}
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new DaoException("Error getting Dao: " . $idao);
		}
		
		// return dao proxy for dao requested
		return $proxy;
	}

}
?>
<?php
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';
require_once 'db/DaoException.php';
require_once 'db/Factory.php';

/**
 * It's the loader class to parse a Dao configuration file.
 * It reads the source path file, instantiting DataSource with it's properties
 * and reading defined dao classes, methods to memoize, putting that in a map ready to be access
 * when a Dao instance is required
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package common;
 * @subpackage dao;
 * @since PHP 5.1;
 * @date January 2008;
 * 
 * @example
 * <?xml version="1.0" encoding="UTF-8"?>
 * <config>
 * 	  <datasource impl="common/db/mysqli/MysqliDataSource">
 * 		 <property name="host">http://www.ceppi.dyn-o-saur.com</property>
 * 		 <property name="port">3306</property>
 * 		 <property name="dbName">test</property>
 * 		 <property name="user">test</property>
 * 		 <property name="passwd">test</property>
 * 	  </datasource>
 * 	  <dao>
 * 		 <iface>app/persistence/iface/IFamilyDao</iface>
 * 		 <impl>app/persistence/impl/FamilyDao</impl>
 * 		 <memoizedMethod>selectByCardId</cachedMethod>
 * 	  </dao>
 * </config>
*/
class DaoLoader {
	
	/**
	 * @var array of string representing dao classes defined in dao config;
	 */
	private $daos = array();
	
	/**
	 * @var DataSource data source defined in dao config;
	 */
	private $dataSource;
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * Creates a DaoLoader based on given file path;
	 *
	 * @param String $filePath dao config file path containing information about data source and dao;
	 */
	public function __construct($filePath) {
		if (self::$logger == null) {
			self::$logger = LoggerManager::getLogger(__CLASS__);
		}
		// parsing dao config file
		try {
			// reading file
			self::$logger->info("Reading Dao config file: " . $filePath);
			$doc = new DOMDocument();
			$doc->load($filePath);

			$this->readDataSource($doc);
			$this->readDaoMap($doc);
		
		} catch (Exception $e) {
			$message = "Error reading daoConfig file: $filePath";
			self::$logger->error($e->getMessage());
			throw new DaoException($message);
		}
	
	}
	
	/**
	 * Read and parse the sub-section of dao config file about the DataSource and its properties,
	 * setting read information in the $this->dataSource variable;
	 *
	 * @param DOMDocument $doc: its the xml representation for the dao config;
	 */
	private function readDataSource($doc) {
		
		// select datasource tag
		$dataSourceNode = $doc->getElementsByTagName("datasource")->item(0);
		
		// select attribute impl, representing the class to instance
		$dataSourceClass = $dataSourceNode->getAttribute("impl");
		
		// datasource object
		self::$logger->info("DataSource class: " . $dataSourceClass);
		
		// select properties tag
		$properties = $dataSourceNode->getElementsByTagName("property");
		
		$args = array();
		
		foreach ($properties as $property) {
			
			// select name and value of the property
			$propertyName = $property->getAttribute("name");
			$propertyValue = $property->nodeValue;
            if ($propertyName != "passwd")
			     self::$logger->info("Setting DataSource property: $propertyName = '$propertyValue'");
			
			// add argument
			array_push($args, $propertyValue);
		}
		
		// instance object
		$this->dataSource = Factory::newInstance($dataSourceClass, $args);
    
	}
	
	/**
	 * Read and parse the sub-section of dao config file about the Daos objects,
	 * setting read information in the $this->daos variable;
	 *
	 * @param DOMDocument $doc: its the xml representation for the dao config;
	 */
	private function readDaoMap($doc) {
		
		// select dao tags
		$daos = $doc->getElementsByTagName("dao");
		
		foreach ($daos as $dao) {
			
			// putting each dao string (iface, impl) in the map
			$iface = $dao->getElementsByTagName("name")->item(0)->nodeValue;
			$impl = $dao->getElementsByTagName("impl")->item(0)->nodeValue;
			
			$memoizedMethods = $dao->getElementsByTagName("memoizedMethod");
			
			$methods = array();
			
			$ifaceName = Factory::getClassName($iface);
			
			foreach ($memoizedMethods as $memoizedMethod) {

				$memoizedMethodName = $memoizedMethod->nodeValue;
				
				self::$logger->info("Setting memoized method for class " . $ifaceName . ": method[" . $memoizedMethodName . "];");
				
				// add argument
				array_push($methods, $memoizedMethodName);
			}
			
			$this->daos[$ifaceName]["implementation"] = $impl;
			if (count($methods) > 0){
				$this->daos[$ifaceName]["memoizedMethods"] = $methods;
			}
			
		}
	}
	
	/**
	 * Return the Daos Map;
	 *
	 * @return array: associative array containing a couple (iface string, impl string) for each dao
	 * read from dao config;
	 */
	public function getDaosMap() {
		return $this->daos;
	}
	
	/**
	 * Return the datasource;
	 *
	 * @return DataSource: datasource instantiated from definition of the dao config file;
	 */
	public function getDataSource() {
		return $this->dataSource;
	}

}
?>
<?php

/**
 * Connection to a DataSource, providing methods interfaces for standard Connection operations
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package common;
 * @subpackage db;
 * @since PHP 5.1;
 * @see DataSource;
 * @date April 2008;
 */
interface Connection {
	
	/**
     * Return the TransactionManager for this Connection
     *
     * @return TransactionManager: manager of transactions needed to performs operations on transactions
     * such as commit, rollback...
    */
	function getTransactionManager();
	
	/**
     * Return if the connection is closed;
     *
     * @return bool: true if the connection is closed;
    */
	function isClosed();
			
	/**
     * Close the Connection. To execute further operations on DataSource will be needed to obtain a
     * new opened Connection
    */
	function close();
	
}

/**
 * DataSource interface representing a generic data source;
 * implementations of this interface can be of any type (database, xml...)
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package common;
 * @subpackage db;
 * @since PHP 5.1;
 * @date April 2008;
 */
Interface DataSource{

	/**
     * Return a connection to datasource; a connection to a data source depends from the DataSource type;
     *
     * @return Connection: object representing a connection to this DataSource needed to execute operations on it;
    */
	function createConnection();
}

/**
 * QueryRunner is the object that physically can execute operations on data of a data source;
 * We can so consider it like a data handler; it provides methods to performs operations described
 * by statements, given from user, and args if needed;
 * Data operation that modify data on data source without return anything can be generally named
 * statement executions; instead operations on sata source that need a data return can be named query;
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package common;
 * @subpackage db;
 * @since PHP 5.1;
 * @see TransactionManager;
 * @date April 2008;
 */
interface QueryRunner {
	
	/**
	 * Performs a query on data source; some result (even an empty one) is expected;
	 * 
	 * @param string $stmt: it represent the query to perform
	 * @param array $args: arguments for the query to perform; type of arguments depends by the implementation;
	 * @return array associative containing name of fields as keys and value of fields as value of array
	 */
	function query($stmt, array $args = array());
	
	/**
	 * Performs a statement execution on data source; some result explaining execution could anyway be returned;
	 * 
	 * @param string $stmt: it represent the operation to perform
	 * @param array $args: arguments for the operation to perform; type of arguments depends by the implementation;
	 * 		Arguments could be an array of arrays, in that case batch execution of the prepared statement is performed;
	 * @return last inserted id if the information is available;
	 */
	function execute($stmt, array $args = array());

}

/**
 * TransactionManager able to performs operation on transaction of a Connection;
 * Inside a connection can be only an active transaction, but more than one transaction can be
 * executed during a Connection, but not at the same time.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package common;
 * @subpackage db;
 * @since PHP 5.1;
 * @see Connection;
 * @date April 2008;
 */
interface TransactionManager {

	/**
     * Starts a new Transaction if no Transactions are active at the time;
    */
	function startTransaction();
	
	/**
     * Commit the current transaction;
    */
	function commit();
	
	/**
     * Rollback the current transaction;
    */
	function rollback();
	
	/**
     * @return QueryRunner to perform data operations;
    */
	function getQueryRunner();

}
?>
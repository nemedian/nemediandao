<?php

require_once 'db/MemoizerProxy.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';
require_once 'db/DaoException.php';
require_once 'log4php/LoggerManager.php';

/**
 * DaoProxy is the manager of transactions for dao object. Calls to dao proxed are intercepted by daoProxy that silently
 * begins and ends transaction when required; it provides decoupling between user data operations and data source.
 * DaoProxy extends MemoizerProxy e so it inherits all caching functionality offered.
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package common;
 * @subpackage dao;
 * @since PHP 5.1;
 * @see MemoizerProxy
 * @date January 2009;
 */
class DaoProxy extends MemoizerProxy {
	
	/**
	 * @var LoggerCategory: logger for class;
	 */
	private static $logger;
	
	/**
	 * @var TransactionManager it's the class that physically permits to DaoProxy to manage transactions.
	 */
	private $transactionManager;
	
	/**
	 * Constructor: creates a DataAccess Object Proxy based on given TransactionManager and proxed Dao;
	 *
	 * @param Dao $dao it's the dao object proxed to which data operations are requested; 
	 * @param TransactionManager $transactionManager it's the transaction handler that physically permits
	 * to DaoProxy to manage transactions.
	 */
	public function __construct(Dao $dao, TransactionManager $transactionManager) {
		parent::__construct($dao);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->transactionManager = $transactionManager;
	}
	
	/**
	 * Method that contains operations executed before public method is called from outside on DaoProxy; It permits
	 * to execute standard operatios needed before the call is transfered to proxed dao; for now it starts the transaction;
	 * 
	 * @param string $name name of the method to invoke to proxed dao;
	 * @param array $arguments arguments to invoke the methode with;
	 * @see Proxy::begin()
	 */
	protected function begin($name, $arguments) {
		self::$logger->debug('begin call; type: ' . get_class($this->o) . '; method: [' . $name . ']; args: [' . $this->serialize($arguments) . ']');
		try {
			$this->transactionManager->startTransaction();
			self::$logger->debug("Start transaction");
		} catch (Exception $e) {
			$message = "Error starting transaction";
			self::$logger->error($e->getMessage());
			throw new DaoException($message);
		}
	}
	
	/**
	 * Method that contains operations executed after every public method is called from outside on DaoProxy; It permits
	 * to execute standard operatios needed after the call to proxed dao; for now it end the transaction;
	 * 
	 * @param string $name name of the method invoked to proxed dao;
	 * @param array $arguments arguments invoked the methode with;
	 * @param object $result result of invoked method
	 * @see Proxy::end()
	 */
	protected function end($name, $arguments, $result) {
		self::$logger->debug('end call; ' . get_class($this->o) . '; method: [' . $name . ']; args: [' . $this->serialize($arguments) . ']; result: [' . $this->serialize($result) . '];');
		try {
			$this->transactionManager->commit();
			self::$logger->debug("End transaction");
		} catch (Exception $e) {
			$message = "Error ending transaction";
			self::$logger->error($e->getMessage());
			throw new DaoException($message);
		}
	}
	
	/**
	 * Method that contains operations executed if an exception occur in the method of dao object proxed by DaoProxy; It permits
	 * to execute standard operatios and centralize error handling in dao objects (if properly error handler and exception handler 
	 * are provided is it possible for proxed dao methods not to raise any Exception, cause DaoProxy could be able to wrap any error
	 * generated in a generic DaoException)
	 * 
	 * @param string $name: name of the method invoked to proxed dao;
	 * @param array $arguments: arguments invoked the methode with;
	 * @param Exception $e: exception raised
	 * @return DaoException
	 * @see Proxy::exception()
	 */
	protected function exception($name, $arguments, Exception $e) {
		parent::exception($name, $arguments, $e);
		$this->transactionManager->rollback();
		return new DaoException("Exception executing method.");
	}

}
?>
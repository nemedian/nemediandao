<?php

/**
 * Exception thrown if an error concerning data access is raised
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package db;
 * @subpackage mysqli;
 * @since PHP 5.1;
 * @see Exception
 * @date April 2007;
 */
class DatabaseException extends Exception {
	
	/**
	 * Constructor: creates a DatabaseException with given parameters;
	 *
	 * @param string $message: exception message
	 * @param string $code: user defined exception code
	 */
	public function __construct($message, $code = 0) {
		parent::__construct($message, $code);
	}
	
	/**
	 * @return the string representation for DatabaseException
	 */
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}

}
?>
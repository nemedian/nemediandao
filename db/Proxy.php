<?php

require_once 'log4php/LoggerManager.php';

/**
 * Proxy uses PHP magic method call to implement the proxy pattern.
 * Each proxy instance has an associated proxed object with it's instantiated with.
 * When a method is invoked on a proxy instance, the method invocation is encoded and dispatched
 * to the invoke method of its proxed object. Any object could be proxed.
 * 
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package class;
 * @subpackage proxy;
 * @since PHP 5.1;
 * @date January 2009;
 */
abstract class Proxy {
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/*
	 * @var object proxed object for this proxy instance.
	 */
	protected $o;
	
	/*
	 * Constructs a new Proxy instance from a proxed object
	 * 
	 * @param object $o it's the object proxed to which method are invoked
	 */
	public function __construct($o) {
		$this->o = $o;
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	/**
	 * PHP magic method used to simulate proxy pattern. Name and arguments specify with method
	 * call on proxy object.
	 * This method is automatically called by php when a not existing method is invoked on proxy object
	 * (this permits to work on proxy as you where work directly on proxed object)
	 * Subclasses can override this method to implements different behaviors or inhibits automatic invocation
	 * 
	 * @param string $name name of the method to invoke to proxed object;
	 * @param array $arguments arguments to invoke the methode with;
	 */
    public function __call($name, $arguments) {
		return $this->invoke($name, $arguments);
	}
	
	/**
	 * phisycally invokes requested method on proxed oject
	 * 
	 * @param string $name name of the method to invoke to proxed object;
	 * @param array $arguments arguments to invoke the methode with;
	 */
	protected final function invoke($name, $arguments) {
		
		$toReturn = null;
		
		$this->begin($name, $arguments);
		
		try {
			
			// check arguments type
			$adjusted = $this->adjustArguments($arguments);
			
			eval('$toReturn = $this->o->' . $name . '(' . implode(", ", $adjusted) . ');');
		
		} catch (Exception $e) {
			throw $this->exception($name, $arguments, $e);
		}
		
		$this->end($name, $arguments, $toReturn);
		
		return $toReturn;
	}
	
	/**
	 * Method that contains operations executed before every public method is called from outside on Proxy; It permits
	 * to execute standard operatios needed before the call is transfered to proxed object
	 * 
	 * @param string $name name of the method to invoke on proxed object;
	 * @param array $arguments to invoke the methode with;
	 */
	protected abstract function begin($name, $arguments);
	
	/**
	 * Method that contains operations executed after every public method is called from outside on Proxy; It permits
	 * to execute standard operatios needed after the call to proxed object
	 * 
	 * @param string $name name of the method invoked to proxed object;
	 * @param array $arguments arguments invoked the methode with;
	 * @param object $result result of invoked method
	 */
	protected abstract function end($name, $arguments, $result);
	
	/**
	 * check parameter type and adjust them to be use by proxy
	 * 
	 * @param array $arguments: arguments to adjust
	 * @return array argument adjusted
	 */
	private final function adjustArguments(array $arguments) {
		$adjusted = array();
		$i = 0;
		foreach ($arguments as $arg) {
			if (is_string($arg))
				$arg = "\$arguments[" . $i . "]";
			if (is_object($arg))
				$arg = "\$arguments[" . $i . "]";
			if (is_array($arg))
				$arg = "\$arguments[" . $i . "]";
			if (is_null($arg))
				$arg = "null";
			array_push($adjusted, $arg);
			$i++;
		}
		return $adjusted;
	}
	
	/**
	 * Method that contains operations executed if an exception occur in the method of object proxed by Proxy; It permits
	 * to execute standard operatios and centralize error handling in objects
	 * 
	 * @param string $name: name of the method invoked on proxed object;
	 * @param array $arguments: arguments invoked the methode with;
	 * @param Exception $e: exception raised
	 * @return Exception
	 */
	protected function exception($name, $arguments, Exception $e) {
		self::$logger->error(get_class($e) . ' in: ' . get_class($this->o) . '; method: [' . $name . ']; args: [' . $this->serialize($arguments) . ']; ' . $e->getMessage());
		return $e;
	}
	
	/**
	 * method used to serialize any type of argument for loggin purposes. Method is recursive so
	 * that is able to serialize nested data like arrays
	 * 
	 * @param mixed $toSerialize: arguments to adjust
	 * @return string serialized object
	 */
	protected function serialize($toSerialize = null) {
		
		static $SPACER = ", ";
		
		if (is_string($toSerialize)) {
			return $toSerialize;
		} else if (is_object($toSerialize)) {
			return "Object(" . get_class($toSerialize) . ")";
		} else if (is_array($toSerialize)) {
			$coma = false;
			$array = "Array(";
			foreach ($toSerialize as $val) {
				if ($coma) {
					$array .= $SPACER;
				}
				$array .= $this->serialize($val);
				$coma = true;
			}
			$array .= ")";
			return $array;
		} else if (is_null($toSerialize)) {
			return "null";
		}
		return $toSerialize;
	}

}
?>
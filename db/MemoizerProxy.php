<?php

require_once 'mutex/Locker.php';
require_once 'mutex/MutexIdentity.php';
require_once 'cache/CacheManager.php';
require_once 'db/Proxy.php';
require_once 'log4php/LoggerManager.php';

/**
 * MemoizerProxy is an extension of proxy that impelments the memoizer pattern to
 * perform caching of method results. It is able to work with Cache implementation that
 * provide memory support to storage of data.
 * If any method need to be cached it's beaviour is the same of superclass Proxy.
 * 
 * During is execution it requires an 'id' to identify witch Cache type request to CacheManager
 * If no id is provided the sessionId is requested to SessionHandler and that id is used as personal
 * cache id. To avoid the runtime dependency to SessionHandler be sure to provide a valid cacheId
 * 
 * To properly instantiate the MemoizerProxy, the global constant CACHE_CONFIG_PATH must be defined
 * as a correct source to instantiate the CacheManager, need to correctly work
 * 
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.2;
 * @package class;
 * @subpackage proxy;
 * @since PHP 5.1;
 * @see Cache
 * @see CacheManager
 * @see SessionHandler
 * @date January 2009;
 */
abstract class MemoizerProxy extends Proxy {
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * @var CacheManager to request cache
	 */
	protected static $cacheManager;
	
	/**
	 * @var array methods to cache. If a method is not in this array, nothing will be performed on it
	 */
	private $memoizedMethods = array();
	
	protected static $CACHE_NAME = "MemoizerProxy";
	
	/**
	 * Constructor: creates a Memoizer Proxy based on given object
	 *
	 * @param object $o it's the object proxed to which method are invoked (and memoized) 
	 */
	public function __construct($o) {
		parent::__construct($o);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		if (self::$cacheManager == null) {
			self::$cacheManager = CacheManager::getInstance(CACHE_CONFIG_PATH);
		}
	}
	
	/**
	 * PHP magic method used to simulate proxy pattern. Name and arguments specify with method
	 * call on proxy object.
	 * This method is automatically called by php when a not existing method is invoked on proxy object
	 * (this permits to work on proxy as you where work directly on proxed object)
	 * 
	 * @param string $name name of the method to invoke to proxed object;
	 * @param array $arguments arguments to invoke the methode with;
	 * @see Proxy::__call()
	 */
	public function __call($name, $arguments) {
		
		$toReturn = null;
		
		try {
			
			if (in_array($name, $this->memoizedMethods)) {
				
				$toReturn = $this->invokeMemoizedMethod($name, $arguments);
			
			} else {
				
				/* method not to memoize, perform normal call */
				$toReturn = parent::invoke($name, $arguments);
			}
		
		} catch (Exception $e) {
			throw $this->exception($name, $arguments, $e);
		}
		
		return $toReturn;
	
	}
	
	protected function invokeMemoizedMethod($name, $arguments) {
		
		// retrieve cache
		/* @var $cache Cache */
		$cacheId = get_class($this->o) . "::" . $name;
		$cache = self::$cacheManager->getCache(self::$CACHE_NAME, $cacheId);
		
		$mutexIdentity = new MutexIdentity($cacheId);
		$key = $this->generateKey($name, $arguments);
		
		try {
			
			$this->lock($mutexIdentity);
			$toReturn = $cache->get($key);
			if ($toReturn == null && ! $cache->containsKey($key)) {
				$toReturn = parent::invoke($name, $arguments);
				$cache->put($key, $toReturn);
				self::$logger->debug('Method result cached. ' . $key . '];');
			} else {
				self::$logger->debug('Method result retrieved from cache. ' . $key . '];');
			}
			$this->unlock($mutexIdentity);
			
		} catch (Exception $e) {
			// release mutex and end critical section
			$this->unlock($mutexIdentity);
			throw $e;
		}
		
		// a copy is returned
		return unserialize(serialize($toReturn));
	
	}
	
	/**
	 * set the array of CachedMethod
	 * 
	 * @param array $cachedMethods of string arguments to invoke the methode with;
	 */
	public final function setMemoizedMethods(array $memoizedMethods) {
		foreach ($memoizedMethods as $memoizedMethod) {
			$this->memoizedMethods[] = $memoizedMethod;
		}
	}
	
	/**
	 * generate the internal key to store result of method memoizing
	 * 
	 * @param string $name name of the method to invoke to proxed object;
	 * @param array $arguments arguments to invoke the methode with;
	 */
	protected function generateKey($name, $arguments) {
		return get_class($this->o) . "::" . $name . "(" . serialize($arguments) . ")";
	}
	
	protected function lock(MutexIdentity $mutexId) {
		Locker::getInstance()->lock($mutexId);
	}
	
	protected function unlock(MutexIdentity $mutexId) {
		Locker::getInstance()->unlock($mutexId);
	}

}
?>
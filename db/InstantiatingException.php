<?php

/**
 * Exception thrown if an error instantiation is raised
 *
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package class;
 * @subpackage factory;
 * @since PHP 5.1;
 * @see Exception
 * @date April 2007;
 */
class InstantiatingException extends Exception {
	
}
?>
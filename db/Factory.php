<?php

require_once 'db/InstantiatingException.php';
require_once 'log4php/LoggerManager.php';

/**
 * Factory class able to instantiate any instantiable object.
 * Class Performs two operations.
 * 1. include the file that contain class object definition to instance
 * 2. instance the class
 * 
 * The fully qualified name of the class to instance must be provided in order to
 * permit this operations.
 * 
 * 
 * @author Giampaolo Grieco <giampaolo.grieco@gmail.com>,  Pierluigi Taddei <perluigi.taddei@gmail.com>
 * @version 1.1;
 * @package class;
 * @subpackage factory;
 * @since PHP 5.1;
 * @date April 2008;
 */
class Factory {
	
	/**
	 * @var LoggerCategory logger for class;
	 */
	private static $logger;
	
	/**
	 * Creates a new instance of the class represented by this class object.
	 * The class is instantiated as if by a new expression given argument list.
	 *
	 * @param string $class fully qualified name of the file to include and class to instance
	 * @param string $args parameters of class construnctor to be provided to instance the object
	 */
	public static function newInstance($class, array $args = array()) {
		
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		
		try {
			
			$class = trim($class);
			
			$toInstance = self::getClassName($class);
			
			require_once $class . '.php';
			
			self::$logger->debug("Instance object: " . $toInstance . " from " . $class . ".php");
			$rc = new ReflectionClass($toInstance);
			$instance = empty($args) ? $rc->newInstance() : $rc->newInstanceArgs($args);
		
		} catch (Exception $e) {
			self::$logger->error($e->getMessage());
			throw new InstantiatingException($e->getMessage());
		}
		
		return $instance;
	}
	
	/**
	 *  Returns the name of the entity (class, interface, array class, primitive type) represented by this class object, as a String.
	 *
	 * @param string $class fully qualified name of the file hat shoud represent a class object
	 */
	public static function getClassName($class) {
		if (strpos($class, "/") === false) {
			$pos = 0;
		} else {
			$pos = strripos($class, "/") + 1;
		}
		return substr($class, $pos);
	}
}

?>